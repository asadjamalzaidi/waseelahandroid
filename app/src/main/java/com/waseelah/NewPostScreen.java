package com.waseelah;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by User on 8/28/2016.
 */
public class NewPostScreen extends AppCompatActivity {
    private Context context;
    private View toolbarView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_post_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = NewPostScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView textTopLeft = (TextView)toolbarView.findViewById(R.id.textTopLeft);
        textTopLeft.setVisibility(View.VISIBLE);
        textTopLeft.setText("Post");
    }
}
