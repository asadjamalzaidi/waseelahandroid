package com.waseelah.Model;

/**
 * Created by User on 9/11/2016.
 */
public class AnjumanListModel {
    private String name, uuid,
            desiredLocationName, UserName, ProfileImage, CoverImage,
            RegistrationId, Secretary, Nauhakhwan, Sadar, RegistrationDate, FollowersId;
    private int desiredLocationId, UserId, Followers;

    public String getFollowersId() {
        return FollowersId;
    }

    public void setFollowersId(String followersId) {
        FollowersId = followersId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
    }

    public String getSadar() {
        return Sadar;
    }

    public void setSadar(String sadar) {
        Sadar = sadar;
    }

    public String getNauhakhwan() {
        return Nauhakhwan;
    }

    public void setNauhakhwan(String nauhakhwan) {
        Nauhakhwan = nauhakhwan;
    }

    public String getSecretary() {
        return Secretary;
    }

    public void setSecretary(String secretary) {
        Secretary = secretary;
    }

    public String getRegistrationId() {
        return RegistrationId;
    }

    public void setRegistrationId(String registrationId) {
        RegistrationId = registrationId;
    }

    public int getFollowers() {
        return Followers;
    }

    public void setFollowers(int followers) {
        Followers = followers;
    }

    public String getCoverImage() {
        return CoverImage;
    }

    public void setCoverImage(String coverImage) {
        CoverImage = coverImage;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getDesiredLocationName() {
        return desiredLocationName;
    }

    public void setDesiredLocationName(String desiredLocationName) {
        this.desiredLocationName = desiredLocationName;
    }

    public int getDesiredLocationId() {
        return desiredLocationId;
    }

    public void setDesiredLocationId(int desiredLocationId) {
        this.desiredLocationId = desiredLocationId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
