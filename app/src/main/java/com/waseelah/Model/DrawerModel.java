package com.waseelah.Model;

/**
 * Created by training on 19/08/16.
 */

public class DrawerModel {
    private int drawableId, counterValue;
    private String name;

    public DrawerModel(int drawableId, String title, int counterValue) {
        super();

        this.name = title;
        this.drawableId = drawableId;
        this.counterValue = counterValue;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getCounterValue() {
        return counterValue;
    }

    public void setCounterValue(int counterValue) {
        this.counterValue = counterValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
