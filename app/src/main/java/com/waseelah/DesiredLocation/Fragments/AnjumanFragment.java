package com.waseelah.DesiredLocation.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.android.volley.VolleyError;
import com.waseelah.Adapter.AnjumanListRecyclerAdapter;
import com.waseelah.Model.AnjumanListModel;
import com.waseelah.Model.PostModel;
import com.waseelah.R;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by User on 9/11/2016.
 */
public class AnjumanFragment extends Fragment {
    private View containerLayout;
    private Context context;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter rAdapter;
    private LinearLayoutManager rLayoutManager;
    private int DesiredLocationId;
    private ArrayList<AnjumanListModel> anjumanModelArrayList = new ArrayList<AnjumanListModel>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        containerLayout = inflater.inflate(R.layout.anjuman_fragment, container, false);
        context = getActivity();
        View customView = inflater.inflate(R.layout.toolbar, container, false);
        DesiredLocationId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "DesiredLocationId"));
        progressBar = (ProgressBar) containerLayout.findViewById(R.id.progressbar);
        swipeRefreshLayout = (SwipeRefreshLayout) containerLayout.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView = (RecyclerView) containerLayout.findViewById(R.id.recycler_view);

        anjumanModelArrayList = new ArrayList<AnjumanListModel>();
        mRecyclerView.setHasFixedSize(true);
        rLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(rLayoutManager);

        rAdapter = new AnjumanListRecyclerAdapter(context, anjumanModelArrayList);
        mRecyclerView.setAdapter(rAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstVisibleItem, visibleItemCount, totalItemCount;

                firstVisibleItem = rLayoutManager.findFirstVisibleItemPosition();
                totalItemCount = rLayoutManager.getItemCount();
                visibleItemCount = mRecyclerView.getChildCount();

//                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
//                    if (!loadingMoreItems)
//                        getDistinctUsersPosts();
//
//                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        getAnjumanList();


        return containerLayout;
    }

    private void getAnjumanList() {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        String Url = GlobalConfig.BASE_URL + GlobalConfig.GET_ALL_ANJUMAN;
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("DesiredLocationId", DesiredLocationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url = Url + "?DesiredLocationId=" + DesiredLocationId;

        APIManager.jsonArrayVolleyRequest(context, Url, "GET", jsonParams);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {

            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {

            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {
                Log.i("Response: ", response.toString());
                setListItemsInModel(response);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }
                APIManager.handleErrorResponseOfRequest(context, error, "Server Error", "GetAllPost");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });
    }

    private void setListItemsInModel(JSONArray jsonArray) {
        String blank = "";
        try {
            for (int i = 0; i < jsonArray.length(); i++) {

                AnjumanListModel postModel = new AnjumanListModel();

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                if (jsonObject.has("uuid") && !jsonObject.isNull("uuid")) {
                    postModel.setUuid(jsonObject.getString("uuid"));
                } else
                    postModel.setUuid(blank);

                if (jsonObject.has("Name") && !jsonObject.isNull("Name"))
                    postModel.setName(jsonObject.getString("Name"));
                else
                    postModel.setName(blank);

                if (jsonObject.has("DesiredLocationId") && !jsonObject.isNull("DesiredLocationId"))
                    postModel.setDesiredLocationId(jsonObject.getInt("DesiredLocationId"));
                else
                    postModel.setDesiredLocationId(0);

                if (jsonObject.has("DesiredLocationName") && !jsonObject.isNull("DesiredLocationName"))
                    postModel.setDesiredLocationName(jsonObject.getString("DesiredLocationName"));
                else
                    postModel.setDesiredLocationName(blank);

                if (jsonObject.has("UserId") && !jsonObject.isNull("UserId"))
                    postModel.setUserId(jsonObject.getInt("UserId"));
                else
                    postModel.setUserId(0);

                if (jsonObject.has("UserName") && !jsonObject.isNull("UserName"))
                    postModel.setUserName(jsonObject.getString("UserName"));
                else
                    postModel.setUserName(blank);

                if (jsonObject.has("ProfileImage") && !jsonObject.isNull("ProfileImage"))
                    postModel.setProfileImage(jsonObject.getString("ProfileImage"));
                else
                    postModel.setProfileImage(blank);

                if (jsonObject.has("CoverImage") && !jsonObject.isNull("CoverImage"))
                    postModel.setCoverImage(jsonObject.getString("CoverImage"));
                else
                    postModel.setCoverImage(blank);

                if (jsonObject.has("FollowersId") && !jsonObject.isNull("FollowersId"))
                    postModel.setFollowersId(jsonObject.getString("FollowersId"));
                else
                    postModel.setFollowersId(blank);

                if (jsonObject.has("Followers") && !jsonObject.isNull("Followers"))
                    postModel.setFollowers(jsonObject.getInt("Followers"));
                else
                    postModel.setFollowers(0);

                if (jsonObject.has("Details") && !jsonObject.isNull("Details")){
                    JSONObject jsonObject1 =  new JSONObject(jsonObject.getString("Details"));

                    if (jsonObject1.has("SecretaryName") && !jsonObject1.isNull("SecretaryName"))
                        postModel.setSecretary(jsonObject1.getString("SecretaryName"));
                    else
                        postModel.setSecretary(blank);

                    if (jsonObject1.has("NauhakhwanName") && !jsonObject1.isNull("NauhakhwanName"))
                        postModel.setNauhakhwan(jsonObject1.getString("NauhakhwanName"));
                    else
                        postModel.setNauhakhwan(blank);

                    if (jsonObject1.has("SadarName") && !jsonObject1.isNull("SadarName"))
                        postModel.setSadar(jsonObject1.getString("SadarName"));
                    else
                        postModel.setSadar(blank);
                }
                else{
                    postModel.setSecretary(blank);
                    postModel.setNauhakhwan(blank);
                    postModel.setSadar(blank);
                }

                if (jsonObject.has("RegistrationId") && !jsonObject.isNull("RegistrationId"))
                    postModel.setRegistrationId(jsonObject.getString("RegistrationId"));
                else
                    postModel.setRegistrationId(blank);

                if (jsonObject.has("RegistrationDate") && !jsonObject.isNull("RegistrationDate"))
                    postModel.setRegistrationDate(jsonObject.getString("RegistrationDate"));
                else
                    postModel.setRegistrationDate(blank);

                anjumanModelArrayList.add(postModel);
            }

            // nearbyGp=nearbyGroupArrayList;

            rAdapter.notifyDataSetChanged();
            // offset = offset + jsonArray.length();

        } catch (Exception e) {
            e.printStackTrace();
        }
        // loadingMoreItems = false;
        // GlobalClass.dismissProgressDialog(progressDialog);
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }

    }
}
