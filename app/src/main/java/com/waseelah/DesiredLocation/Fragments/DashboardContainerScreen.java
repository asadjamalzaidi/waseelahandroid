package com.waseelah.DesiredLocation.Fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waseelah.R;
import com.waseelah.Utilities.HomeSlidingTabLayout;
import com.waseelah.Utilities.HomeViewPagerAdapter;

/**
 * Created by training on 19/08/16.
 */

public class DashboardContainerScreen extends Fragment {
    HomeViewPagerAdapter adapter;
    View containerLayout;
    ViewPager pager;
    CharSequence Titles[] = {"Home", "Program", "Calender"};
    int Numboftabs = 3;
    HomeSlidingTabLayout tabs;
    Context context;
    private int[] drawablesIds = {
            R.drawable.ic_home,
            R.drawable.ic_program,
            R.drawable.ic_calendar
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        containerLayout = inflater.inflate(R.layout.dashboard_fragment, container, false);
        context = container.getContext();
        View customView = inflater.inflate(R.layout.toolbar, container, false);

        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.tool_bar);
        toolbar.addView(customView);

        adapter = new HomeViewPagerAdapter(getActivity().getSupportFragmentManager(), Titles, Numboftabs,getActivity());
        pager = (ViewPager) containerLayout.findViewById(R.id.pager);

        pager.setAdapter(adapter);

        //Assigning the sliding layout tab View

        tabs = (HomeSlidingTabLayout) containerLayout.findViewById(R.id.tabs);
        tabs.setIconResourceArray(drawablesIds);
        tabs.setDistributeEvenly(true); //This makes the tab width evenly in the given space.

        tabs.setViewPager(pager);
        tabs.setCustomTabColorizer(new HomeSlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(android.R.color.white);
            }

        });


        return containerLayout;

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }
}
