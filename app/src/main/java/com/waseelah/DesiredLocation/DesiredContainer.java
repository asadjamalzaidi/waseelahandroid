package com.waseelah.DesiredLocation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.waseelah.Anjuman.AnjumanWallScreen;
import com.waseelah.Anjuman.PaymentScreen;
import com.waseelah.DesiredLocation.Fragments.DashboardContainerScreen;
import com.waseelah.LoginScreen;
import com.waseelah.Model.AnjumanListModel;
import com.waseelah.Model.DrawerModel;
import com.waseelah.R;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 8/13/2016.
 */
public class DesiredContainer extends AppCompatActivity {
    private String MobileNumber, AccessToken, LastLogin, DesiredLocation, userName;
    private View toolbarView;
    Context context;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ProgressDialog mProgressDialog;
    private static final String TAG = "DesiredContainer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.desired_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);



        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = DesiredContainer.this;

        String DesiredLocationId = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "DesiredLocationId");
        MobileNumber = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "MobileNumber");
        userName = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "FirstName");
        AccessToken = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "AccessToken");
        LastLogin = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "LastLogin");
        DesiredLocation = GlobalConfig.getSharedValuePreference(DesiredContainer.this, "DesiredLocation");

//      -----------------  Notification FCM  -----------------
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        FirebaseMessaging.getInstance().subscribeToTopic(DesiredLocation);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, token);
//      -----------------  Notification FCM  -----------------


        TextView toolbar_title = (TextView) toolbarView.findViewById(R.id.toolBarTitle);
        toolbar_title.setTypeface(GlobalConfig.FontType(this, 6));
        toolbar_title.setText(DesiredLocation);
        //  View drawerLayout = getLayoutInflater().inflate(R.layout.side_drawer_layout, toolbar, false);
        TextView user_name = (TextView) findViewById(R.id.user_name);
        TextView user_mobile = (TextView) findViewById(R.id.user_mobile);
        user_name.setTypeface(GlobalConfig.FontType(this, 6));
        user_name.setTypeface(GlobalConfig.FontType(this, 6));
        user_name.setText(userName);
        user_mobile.setText(MobileNumber);

        ListView mDrawerList = (ListView) findViewById(R.id.navListTop);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_admin);

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }


        setupDrawer();


        ArrayList<DrawerModel> arrayModel = new ArrayList<>();
        DrawerModel Dashboard = new DrawerModel(R.drawable.ic_dashboard, "Dashboard", 0);
        DrawerModel Anjuman = new DrawerModel(R.drawable.ic_anjuman, "Anjuman", R.drawable.ic_down);
        DrawerModel Society = new DrawerModel(R.drawable.ic_society, "Society", 0);
        DrawerModel Masjid = new DrawerModel(R.drawable.ic_masjid, "Masjid", 0);
        DrawerModel Settings = new DrawerModel(R.drawable.ic_setting, "Settings", 0);
        DrawerModel Logout = new DrawerModel(R.drawable.ic_logout, "Logout", 0);

        arrayModel.add(Dashboard);
        arrayModel.add(Anjuman);
        arrayModel.add(Society);
        arrayModel.add(Masjid);
        arrayModel.add(Settings);
        arrayModel.add(Logout);

        DrawerAdapterAdmin mAdapter = new DrawerAdapterAdmin(this, arrayModel);
        mDrawerList.setAdapter(mAdapter);
        final String drawerFragments[] = {"com.waseelah.DesiredLocation.Fragments.DashboardContainerScreen",
                "com.waseelah.DesiredLocation.Fragments.AnjumanFragment",
                "com.waseelah.DesiredLocation.Fragments.SocietyFragment",
                "com.waseelah.DesiredLocation.Fragments.MasjidFragment",
                "com.waseelah.DesiredLocation.Fragments.SettingsFragment"};

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mDrawerLayout.closeDrawers();

                if (i < drawerFragments.length) {
                    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.contentContainer, Fragment.instantiate(DesiredContainer.this, drawerFragments[i]))
                            .addToBackStack(null)
                            .commit();
                }

                switch (i) {
                    case GlobalConfig.DASHBOARD:
                        break;

                    case GlobalConfig.ANJUMAN:
                        break;

                    case GlobalConfig.SOCIETY:
                        break;

                    case GlobalConfig.MASJID:
                        break;

                    case GlobalConfig.SETTINGS:
                        break;

                    case GlobalConfig.LOGOUT:
                        logoutUser();
                        break;
                }

            }
        });


        Fragment fragment = new DashboardContainerScreen();

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.contentContainer, fragment)
                .addToBackStack(null)
                .commit();

        mDrawerLayout.closeDrawers();

        String anjumanData = GlobalConfig.getSharedValuePreference(context, "AnjumanData");
        if (anjumanData == null) {
            getAnjumanData();
        }
    }

    private void logoutUser() {
        SharedPreferences sharedPreferences = getSharedPreferences("WaseelahPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear().commit();
        Intent n = new Intent(this, LoginScreen.class);
        startActivity(n);
        finish();
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerAdapterAdmin extends BaseAdapter {

        private Context context;

        private List<DrawerModel> listModel;
        private LayoutInflater inflater;

        public static final int TYPE_LOGOUT = 9;

        private DrawerAdapterAdmin(Context context, List<DrawerModel> listModel) {
            this.listModel = listModel;
            this.inflater = LayoutInflater.from(context);
            this.context = context;
        }

        public int getCount() {
            return listModel.size();
        }

        public DrawerModel getItem(int position) {
            return listModel.get(position);
        }

        public long getItemId(int position) {
            return listModel.get(position).getDrawableId();
        }

        public View getView(int position, View convertView, ViewGroup parent) {


            int listViewItemType = getItemViewType(position);
            ViewHolder holder;

            DrawerModel modelR = listModel.get(position);

            holder = new ViewHolder();

            if (convertView == null) {
                convertView = this.inflater.inflate(R.layout.drawer_list_item, parent, false);

                holder.imgIcon = (CircleImageView) convertView.findViewById(R.id.menu_icon);
                holder.menuName = (TextView) convertView.findViewById(R.id.menu_name);
                holder.menuCounter = (ImageView) convertView.findViewById(R.id.menu_counter);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String menuName = "    " + modelR.getName();

            holder.menuName.setText(menuName);
//            if (modelR.getCounterValue() != 0){
//                String counter = modelR.getCounterValue() +"";
//                holder.menuCounter.setText(counter);
//            }
            if (modelR.getCounterValue() != 0) {
                holder.menuCounter.setImageDrawable(ContextCompat.getDrawable(context, modelR.getCounterValue()));
                holder.menuCounter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            showAlertListDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            holder.menuName.setTypeface(GlobalConfig.FontType(context, 6));
            modelR.getDrawableId();
            Glide.with(DesiredContainer.this).load("").placeholder(modelR.getDrawableId()).animate(android.R.anim.fade_in)
                    .signature(new StringSignature(UUID.randomUUID().toString()))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail((float) 1.0)
                    .into(holder.imgIcon);


            return convertView;
        }

        private class ViewHolder {
            TextView menuName;
            CircleImageView imgIcon;
            ImageView menuCounter;
        }
    }

    private void getAnjumanData() {
        int DesiredLocationId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "DesiredLocationId"));
        int UserId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "UserId"));
        String Url = GlobalConfig.BASE_URL + GlobalConfig.GET_ALL_ANJUMAN_FOR_OFFICIALS;
        JSONObject obj = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
            obj.put("DesiredLocationId", DesiredLocationId);
            obj.put("UserId", UserId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url = Url + "?UserId=" + UserId + "&DesiredLocationId=" + DesiredLocationId;
        APIManager.jsonArrayVolleyRequest(context, Url, "GET", obj);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {

            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {

            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {
                Log.e("response", response + "");

                storeAnjumanSharedPreferenceValues(response);
            }

            @Override
            public void onErrorResponse(VolleyError error) {

                APIManager.handleErrorResponseOfRequest(context, error, "Server Error", "CreateAnjumanScreen");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });
    }

    private void storeAnjumanSharedPreferenceValues(JSONArray jsonResponse) {
        String blank = "";
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < jsonResponse.length(); i++) {
            try {
                AnjumanListModel postModel = new AnjumanListModel();
                JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = jsonResponse.getJSONObject(i);
                if (jsonObject.has("Name") && !jsonObject.isNull("Name")) {
                    jsonObject1.put("Name", jsonObject.getString("Name"));
                } else {
                    jsonObject1.put("Name", blank);
                }
                if (jsonObject.has("ProfileImage") && !jsonObject.isNull("ProfileImage")) {
                    jsonObject1.put("ProfileImage", jsonObject.getString("ProfileImage"));
                } else {
                    jsonObject1.put("ProfileImage", blank);
                }
                if (jsonObject.has("DesiredLocationId") && !jsonObject.isNull("DesiredLocationId")) {
                    jsonObject1.put("DesiredLocationId", jsonObject.getInt("DesiredLocationId"));
                } else {
                    jsonObject1.put("DesiredLocationId", blank);
                }
                if (jsonObject.has("Followers") && !jsonObject.isNull("Followers")) {
                    jsonObject1.put("Followers", jsonObject.getInt("Followers"));
                } else {
                    jsonObject1.put("Followers", blank);
                }
                if (jsonObject.has("PaymentStatus") && !jsonObject.isNull("PaymentStatus")) {
                    jsonObject1.put("PaymentStatus", jsonObject.getInt("PaymentStatus"));
                } else {
                    jsonObject1.put("PaymentStatus", blank);
                }
                if (jsonObject.has("AdminId") && !jsonObject.isNull("AdminId")) {
                    jsonObject1.put("AdminId", jsonObject.getInt("AdminId"));
                } else {
                    jsonObject1.put("AdminId", blank);
                }
                jsonArray.put(jsonObject1);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        GlobalConfig.setSharedValuePreference(context, "AnjumanData", jsonArray.toString());
    }

    private void showAlertListDialog() throws JSONException {

        String anjumanData = GlobalConfig.getSharedValuePreference(context, "AnjumanData");
        JSONArray jsonArray = new JSONArray(anjumanData);
        if (jsonArray.length() > 0) {


            AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
            builderSingle.setTitle("My Anjuman List");

            final ArrayAdapter<JSONObject> arrayAdapter = new ArrayAdapter<JSONObject>(
                    context,
                    android.R.layout.select_dialog_singlechoice);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                arrayAdapter.add(jsonObject);
            }

            builderSingle.setNegativeButton(
                    "cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            builderSingle.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            JSONObject jsonObject = arrayAdapter.getItem(which);
                            try {
                                String anjumanName = jsonObject.getString("Name");
                                String paymentStatus = jsonObject.getString("PaymentStatus");
                                int paymentValue = Integer.parseInt(paymentStatus);
                                if (paymentValue == 1){
                                    Intent n = new Intent(context, AnjumanWallScreen.class);
                                    startActivity(n);
                                }else{
                                    Intent n = new Intent(context, PaymentScreen.class);
                                    startActivity(n);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
            builderSingle.show();
        } else {
            Toast.makeText(context, "No anjuman created", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_desired_container, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
