package com.waseelah.DesiredLocation.DashboardFragmentContainer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.gordonwong.materialsheetfab.MaterialSheetFabEventListener;
import com.waseelah.Adapter.HomeScreenRecyclerAdapter;
import com.waseelah.Anjuman.CreateAnjumanScreen;
import com.waseelah.Anjuman.PaymentScreen;
import com.waseelah.ContentVisualizingScreen;
import com.waseelah.DesiredLocation.DesiredContainer;
import com.waseelah.LoginScreen;
import com.waseelah.Model.AnjumanListModel;
import com.waseelah.Model.PostModel;
import com.waseelah.SelectImageScreen;
import com.waseelah.R;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.Fab;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by training on 19/08/16.
 */

public class HomeViewFragment extends Fragment implements View.OnClickListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private Context context;
    private ProgressBar progressBar;
    private int DesiredLocationId, UserId;
    private boolean shouldExecuteOnResume;
    private RecyclerView.Adapter rAdapter;
    private LinearLayoutManager rLayoutManager;
    private ArrayList<PostModel> postModelArrayList;
    private int statusBarColor;
    //    private FloatingActionButton addFAB;
    private MaterialSheetFab materialSheetFab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_desired_fragment, container, false);
        context = getActivity();
        VolleyError error;
        String url = GlobalConfig.BASE_URL + "/posts/postMessage";
        shouldExecuteOnResume = false;
        RelativeLayout layoutAddPost = (RelativeLayout) v.findViewById(R.id.add_post_layout);
        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
//        addFAB = (FloatingActionButton) v.findViewById(R.id.addFAB);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);

        postModelArrayList = new ArrayList<PostModel>();
        mRecyclerView.setHasFixedSize(true);
        rLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(rLayoutManager);

        rAdapter = new HomeScreenRecyclerAdapter(context, postModelArrayList);
        mRecyclerView.setAdapter(rAdapter);

//        addFAB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
        Fab fab = (Fab) v.findViewById(R.id.addFAB);
        View sheetView = v.findViewById(R.id.fab_sheet);
        View overlay = v.findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.colorPrimary);
        int fabColor = getResources().getColor(R.color.colorAccent);

        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay,
                sheetColor, fabColor);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int firstVisibleItem, visibleItemCount, totalItemCount;

                firstVisibleItem = rLayoutManager.findFirstVisibleItemPosition();
                totalItemCount = rLayoutManager.getItemCount();
                visibleItemCount = mRecyclerView.getChildCount();

//                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
//                    if (!loadingMoreItems)
//                        getDistinctUsersPosts();
//
//                }
            }
        });

        // Set material sheet event listener
        materialSheetFab.setEventListener(new MaterialSheetFabEventListener() {
            @Override
            public void onShowSheet() {
                // Save current status bar color
                statusBarColor = getStatusBarColor();
                // Set darker status bar color to match the dim overlay
                setStatusBarColor(getResources().getColor(R.color.theme_primary_dark2));
            }

            @Override
            public void onHideSheet() {
                // Restore status bar color
                setStatusBarColor(statusBarColor);
            }
        });

        // Set material sheet item click listeners
        v.findViewById(R.id.fab_sheet_item_anjuman).setOnClickListener(this);
        v.findViewById(R.id.fab_sheet_item_masjid).setOnClickListener(this);
        v.findViewById(R.id.fab_sheet_item_society).setOnClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        swipeRefreshLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (materialSheetFab.isSheetVisible()) {
                    materialSheetFab.hideSheet();
                }
            }
        });
        DesiredLocationId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "DesiredLocationId"));
        UserId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "UserId"));
        Log.i("Shared: ", DesiredLocationId + " " + UserId + "");

        layoutAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Write your message here.");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent n = new Intent(getActivity(), ContentVisualizingScreen.class);
                                startActivity(n);
//                                jsonObjectMethod(null);
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        getAllPost();
        return v;
    }



    private int getStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getActivity().getWindow().getStatusBarColor();
        }
        return 0;
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(color);
        }
    }


    @Override
    public void onClick(View view) {
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        }
        switch (view.getId()) {
            case R.id.fab_sheet_item_anjuman:
                Intent n = new Intent(context, CreateAnjumanScreen.class);
                startActivity(n);
                break;

            case R.id.fab_sheet_item_masjid:
                Toast.makeText(context, "Masjid", Toast.LENGTH_SHORT).show();
                break;

            case R.id.fab_sheet_item_society:
                Toast.makeText(context, "Society", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldExecuteOnResume) {
            if (postModelArrayList.size() > 0) {
                postModelArrayList.clear();
            }
            getAllPost();
        } else {
            shouldExecuteOnResume = true;
        }
    }

    private void getAllPost() {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        String Url = GlobalConfig.BASE_URL + GlobalConfig.GET_ALL_POST;
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("UserId", UserId);
            jsonParams.put("DesiredLocationId", DesiredLocationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url = Url + "?UserId=" + UserId + "&DesiredLocationId=" + DesiredLocationId;

        APIManager.jsonArrayVolleyRequest(context, Url, "GET", jsonParams);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {

            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {

            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {
                Log.i("Response: ", response.toString());
                setListItemsInModel(response);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                }
                APIManager.handleErrorResponseOfRequest(context, error, "Server Error", "GetAllPost");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });
    }

    private void setListItemsInModel(JSONArray jsonArray) {
        String blank = "";
        try {
            for (int i = 0; i < jsonArray.length(); i++) {

                PostModel postModel = new PostModel();

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                if (jsonObject.has("id") && !jsonObject.isNull("id")) {
                    postModel.setPostId(jsonObject.getInt("id"));
                } else
                    postModel.setPostId(0);

                if (jsonObject.has("Description") && !jsonObject.isNull("Description"))
                    postModel.setDescription(jsonObject.getString("Description"));
                else
                    postModel.setDescription(blank);

                if (jsonObject.has("Image") && !jsonObject.isNull("Image"))
                    postModel.setImageUrl(jsonObject.getString("Image"));
                else
                    postModel.setImageUrl(blank);

                if (jsonObject.has("Category") && !jsonObject.isNull("Category"))
                    postModel.setCategoryName(jsonObject.getString("Category"));
                else
                    postModel.setCategoryName(blank);

                if (jsonObject.has("CategoryId") && !jsonObject.isNull("CategoryId"))
                    postModel.setCategoryId(jsonObject.getInt("CategoryId"));
                else
                    postModel.setCategoryId(0);

                if (jsonObject.has("UserName") && !jsonObject.isNull("UserName"))
                    postModel.setPostedBy(jsonObject.getString("UserName"));
                else
                    postModel.setPostedBy(blank);

                if (jsonObject.has("UserId") && !jsonObject.isNull("UserId"))
                    postModel.setUserId(jsonObject.getInt("UserId"));
                else
                    postModel.setUserId(0);

                if (jsonObject.has("Privacy") && !jsonObject.isNull("Privacy"))
                    postModel.setPrivacy(jsonObject.getString("Privacy"));
                else
                    postModel.setPrivacy(blank);

                if (jsonObject.has("DesiredLocation") && !jsonObject.isNull("DesiredLocation"))
                    postModel.setDesiredLocation(jsonObject.getString("DesiredLocation"));
                else
                    postModel.setDesiredLocation(blank);

                if (jsonObject.has("DesiredLocationId") && !jsonObject.isNull("DesiredLocationId"))
                    postModel.setDesiredLocationId(jsonObject.getInt("DesiredLocationId"));
                else
                    postModel.setDesiredLocationId(0);

                if (jsonObject.has("Status") && !jsonObject.isNull("Status"))
                    postModel.setStatus(jsonObject.getBoolean("Status"));
                else
                    postModel.setStatus(false);

                if (jsonObject.has("PostedTime") && !jsonObject.isNull("PostedTime"))
                    postModel.setPostedAt(jsonObject.getString("PostedTime"));
                else
                    postModel.setPostedAt(blank);

                if (jsonObject.has("VenueId") && !jsonObject.isNull("VenueId"))
                    postModel.setVenueId(jsonObject.getInt("VenueId"));
                else
                    postModel.setVenueId(0);

                if (jsonObject.has("VenueName") && !jsonObject.isNull("VenueName"))
                    postModel.setVenueName(jsonObject.getString("VenueName"));
                else
                    postModel.setVenueName(blank);

                postModelArrayList.add(postModel);
            }

            // nearbyGp=nearbyGroupArrayList;

            rAdapter.notifyDataSetChanged();
            // offset = offset + jsonArray.length();

        } catch (Exception e) {
            e.printStackTrace();
        }
        // loadingMoreItems = false;
        // GlobalClass.dismissProgressDialog(progressDialog);
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }

    }


}
