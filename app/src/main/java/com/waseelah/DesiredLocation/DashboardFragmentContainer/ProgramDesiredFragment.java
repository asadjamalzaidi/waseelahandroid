package com.waseelah.DesiredLocation.DashboardFragmentContainer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waseelah.R;

/**
 * Created by training on 19/08/16.
 */

public class ProgramDesiredFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.program_desired_fragment,container,false);
        return v;
    }
}
