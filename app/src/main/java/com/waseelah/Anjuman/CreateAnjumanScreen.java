package com.waseelah.Anjuman;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.waseelah.ContentVisualizingScreen;
import com.waseelah.R;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 9/10/2016.
 */
public class CreateAnjumanScreen extends AppCompatActivity {
    private EditText anjumanName, secretaryName, nauhakhwanName, sadarName, registrationId;
    private Context context;
    private View toolbarView;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_anjuman_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = CreateAnjumanScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        anjumanName = (EditText) findViewById(R.id.anjumanName);
        secretaryName = (EditText) findViewById(R.id.secretaryName);
        nauhakhwanName = (EditText) findViewById(R.id.nauhakhwanName);
        sadarName = (EditText) findViewById(R.id.sadarName);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        registrationId = (EditText) findViewById(R.id.registrationId);
        assignTextFace();
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = validateDetails();
                if (valid) {
                    createAnjuman();
                }
            }
        });
    }

    private void assignTextFace() {
        anjumanName.setTypeface(GlobalConfig.FontType(this, 6));
        secretaryName.setTypeface(GlobalConfig.FontType(this, 6));
        nauhakhwanName.setTypeface(GlobalConfig.FontType(this, 6));
        sadarName.setTypeface(GlobalConfig.FontType(this, 6));
        registrationId.setTypeface(GlobalConfig.FontType(this, 6));
    }

    private boolean validateDetails() {
        if (anjumanName.getText().toString().equals("")) {
            showToastMessage("Anjuman name cannot be blank");
            return false;
        } else if (secretaryName.getText().toString().equals("")) {
            showToastMessage("Secretary Name cannot be blank");
            return false;
        } else if (nauhakhwanName.getText().toString().equals("")) {
            showToastMessage("Nauhakhwan Name cannot be blank");
            return false;
        } else {
            return true;
        }
    }

    private void showToastMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private void createAnjuman() {
        String Url = GlobalConfig.BASE_URL + GlobalConfig.CREATE_ANJUMAN;
        JSONObject obj = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("SecretaryName", secretaryName.getText().toString());
            jsonObject.put("NauhakhwanName", nauhakhwanName.getText().toString());
            jsonObject.put("SadarName", sadarName.getText().toString());

            obj.put("Name", anjumanName.getText().toString());
            obj.put("DesiredLocationId", Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "DesiredLocationId")));
            obj.put("DesiredLocationName", GlobalConfig.getSharedValuePreference(context, "DesiredLocation"));
            obj.put("UserId", Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "UserId")));
            obj.put("UserFirstName", GlobalConfig.getSharedValuePreference(context, "FirstName"));
            obj.put("Details", jsonObject.toString());
            obj.put("RegistrationId", registrationId.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIManager.jsonObjectVolleyRequest(context, Url, "POST", obj);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {

            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {
                Log.e("response", response + "");
                storeAnjumanSharedPreferenceValues(response);
            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                APIManager.handleErrorResponseOfRequest(context, error, "Server Error", "CreateAnjumanScreen");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });
    }

    private void storeAnjumanSharedPreferenceValues(JSONObject jsonResponse) {
        JSONArray jsonArray = new JSONArray();
        String blank = "";
        JSONObject jsonObject = new JSONObject();
        try {
            if (jsonResponse.has("Name") && !jsonResponse.isNull("Name")) {
                jsonObject.put("Name",jsonResponse.getString("Name"));
            } else {
                jsonObject.put("Name",blank);
            }
            if (jsonResponse.has("DesiredLocationId") && !jsonResponse.isNull("DesiredLocationId")) {
                jsonObject.put("DesiredLocationId", jsonResponse.getInt("DesiredLocationId"));
            } else {
                jsonObject.put("DesiredLocationId", blank);
            }
            if (jsonResponse.has("DesiredLocationName") && !jsonResponse.isNull("DesiredLocationName")) {
                jsonObject.put("DesiredLocationName", jsonResponse.getString("DesiredLocationName"));
            } else {
                jsonObject.put("DesiredLocationName", blank);
            }
            if (jsonResponse.has("UserId") && !jsonResponse.isNull("UserId")) {
                jsonObject.put("UserId", jsonResponse.getInt("UserId"));
            } else {
                jsonObject.put("UserId", blank);
            }
            if (jsonResponse.has("UserName") && !jsonResponse.isNull("UserName")) {
                jsonObject.put("UserName", jsonResponse.getString("UserName"));
            } else {
                jsonObject.put("DesiredLocationName", blank);
            }
            if (jsonResponse.has("Followers") && !jsonResponse.isNull("Followers")) {
                jsonObject.put("Followers", jsonResponse.getInt("Followers"));
            } else {
                jsonObject.put("Followers", blank);
            }
            if (jsonResponse.has("PaymentStatus") && !jsonResponse.isNull("PaymentStatus")) {
                jsonObject.put("PaymentStatus", jsonResponse.getInt("PaymentStatus"));
            } else {
                jsonObject.put("PaymentStatus", blank);
            }
            if (jsonResponse.has("RegistrationId") && !jsonResponse.isNull("RegistrationId")) {
                jsonObject.put("RegistrationId", jsonResponse.getString("RegistrationId"));
            } else {
                jsonObject.put("RegistrationId", blank);
            }
            if (jsonResponse.has("AdminId") && !jsonResponse.isNull("AdminId")) {
                jsonObject.put("AdminId", jsonResponse.getInt("AdminId"));
            } else {
                jsonObject.put("AdminId", blank);
            }

            String anjumanData = GlobalConfig.getSharedValuePreference(context,"AnjumanData");
            if(anjumanData!=null && anjumanData.length()!=0){
                JSONArray jArray = new JSONArray(anjumanData);
                if(jArray.length()>0){
                    jsonArray = jArray;
                }
            }
            jsonArray.put(jsonObject);
            GlobalConfig.setSharedValuePreference(context,"AnjumanData",jsonArray.toString());
            Intent n =new Intent(CreateAnjumanScreen.this,PaymentScreen.class);
            startActivity(n);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
