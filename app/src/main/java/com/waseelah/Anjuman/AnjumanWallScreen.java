package com.waseelah.Anjuman;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.waseelah.R;
import com.waseelah.Utilities.GlobalConfig;

/**
 * Created by User on 9/11/2016.
 */
public class AnjumanWallScreen extends AppCompatActivity {
    private EditText anjumanName, secretaryName, nauhakhwanName, sadarName, registrationId;
    private Context context;
    private View toolbarView;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anjuman_wall_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = AnjumanWallScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        anjumanName = (EditText) findViewById(R.id.anjumanName);
        secretaryName = (EditText) findViewById(R.id.secretaryName);
        nauhakhwanName = (EditText) findViewById(R.id.nauhakhwanName);
        sadarName = (EditText) findViewById(R.id.sadarName);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        registrationId = (EditText) findViewById(R.id.registrationId);
        assignTextFace();
    }

    private void assignTextFace() {
        anjumanName.setTypeface(GlobalConfig.FontType(this, 6));
        secretaryName.setTypeface(GlobalConfig.FontType(this, 6));
        nauhakhwanName.setTypeface(GlobalConfig.FontType(this, 6));
        sadarName.setTypeface(GlobalConfig.FontType(this, 6));
        registrationId.setTypeface(GlobalConfig.FontType(this, 6));
    }
}
