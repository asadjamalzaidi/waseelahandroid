package com.waseelah.Anjuman;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.waseelah.R;
import com.waseelah.Utilities.GlobalConfig;

/**
 * Created by User on 9/11/2016.
 */
public class PaymentScreen extends AppCompatActivity {
    private Context context;
    private View toolbarView;
    private Button btnSubmit;
    private TextView amountAnjumanPayment, pointTitle, points;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = PaymentScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        amountAnjumanPayment = (TextView) findViewById(R.id.amountAnjumanPayment);
        points = (TextView) findViewById(R.id.points);
        pointTitle = (TextView) findViewById(R.id.pointTitle);
        assignTextFace();
    }

    private void assignTextFace() {
        amountAnjumanPayment.setTypeface(GlobalConfig.FontType(this, 6));
        points.setTypeface(GlobalConfig.FontType(this, 6));
        pointTitle.setTypeface(GlobalConfig.FontType(this, 6));
    }
}
