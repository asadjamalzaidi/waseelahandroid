package com.waseelah.Utilities;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by training on 09/08/16.
 */

public class RequestCallback {
    public interface APIRequestCallback {
        void onSuccessResponse(String response);
        void onSuccessJSONResponse(JSONObject response);
        void onSuccessJSONArrayResponse(JSONArray response);
        void onErrorResponse(VolleyError error);

        void statusCode(int statusCode);
    }

}
