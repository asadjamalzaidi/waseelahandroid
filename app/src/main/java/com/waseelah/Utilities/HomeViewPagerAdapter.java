package com.waseelah.Utilities;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.waseelah.DesiredLocation.DashboardFragmentContainer.HomeViewFragment;
import com.waseelah.DesiredLocation.DashboardFragmentContainer.ProgramDesiredFragment;
import com.waseelah.DesiredLocation.DashboardFragmentContainer.CalendarDesiredFragment;
import com.waseelah.R;

/**
 * Created by training on 19/08/16.
 */

public class HomeViewPagerAdapter extends FragmentStatePagerAdapter{

    private int[] drawablesIds = {
            R.drawable.ic_edit,
            R.drawable.ic_edit,
            R.drawable.ic_edit
    };
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    private Context context;
    public HomeViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumbOfTabsumb, Context context) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = NumbOfTabsumb;
        this.context = context;
    }


    //Constructor and other standard funcs...

    public int getDrawableId(int position) {
        //Here is only example for getting tab drawables
        return drawablesIds[position];
    }


    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {
        if (position == 0) //if position is 0 we are returning the First Tab
        {
            HomeViewFragment tab1 = new HomeViewFragment();
            return tab1;
        }
        if (position == 1) {
            ProgramDesiredFragment tab2 = new ProgramDesiredFragment();
            return tab2;
        } else {
            CalendarDesiredFragment tab3 = new CalendarDesiredFragment();
            return tab3;
        }
    }


    //This method returns the number of tabs for the tabs Strip
    @Override
    public int getCount() {
        return NumbOfTabs;
    }


    //This method returns the Title of the Tabs in the tabStrip
    @Override
    public CharSequence getPageTitle(int position) {
//        return Titles[position];
       return "";
    }



}
