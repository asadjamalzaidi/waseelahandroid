package com.waseelah.Utilities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.waseelah.ContentVisualizingScreen;
import com.waseelah.CreateVenueScreen;
import com.waseelah.R;

/**
 * Created by User on 9/3/2016.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String mLatitude = "26.8465", mLongitude = "80.9467";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView submit = (TextView) findViewById(R.id.submit);
        submit.setTypeface(GlobalConfig.FontType(this, 6));
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.submit_layout);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent previousScreen = new Intent(MapActivity.this, CreateVenueScreen.class);
                previousScreen.putExtra("Latitude", mLatitude);
                previousScreen.putExtra("Longitude", mLongitude);
                setResult(123, previousScreen);
                finish();
            }
        });

    }

    @Override
    public void finish() {
        Intent previousScreen = new Intent(MapActivity.this, CreateVenueScreen.class);
        previousScreen.putExtra("Latitude", mLatitude);
        previousScreen.putExtra("Longitude", mLongitude);
        setResult(123, previousScreen);
        super.finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        LatLng placeDefault = new LatLng(26.8465, 80.9467);
        MarkerOptions markerOptions = new MarkerOptions().position(placeDefault).title(
                "Drag the marker").draggable(true);
        mMap.addMarker(markerOptions);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(placeDefault, 5);
        mMap.animateCamera(yourLocation);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point));
                mLatitude = point.latitude + "";
                mLongitude = point.longitude + "";
            }
        });
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.d("TAG", "latitude : " + marker.getPosition().latitude);
                marker.setSnippet(marker.getPosition().latitude + "");
                mLatitude = marker.getPosition().latitude + "";
                mLongitude = marker.getPosition().longitude + "";
                Toast.makeText(MapActivity.this, "Latitude: " + marker.getPosition().latitude + ""
                        + ", Longitude: " + marker.getPosition().longitude + "", Toast.LENGTH_SHORT)
                        .show();
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

        });
    }
}
