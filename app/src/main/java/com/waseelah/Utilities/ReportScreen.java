package com.waseelah.Utilities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.waseelah.LoginScreen;
import com.waseelah.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by training on 10/08/16.
 */

public class ReportScreen extends AppCompatActivity implements View.OnClickListener {
    private EditText firstName, emailAddress, mobileNumber, messageReport;
    private Button btnReport;
    private TextView textLimitValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_screen);

        firstName = (EditText) findViewById(R.id.firstName);
        emailAddress = (EditText) findViewById(R.id.emailAddress);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        messageReport = (EditText) findViewById(R.id.messageReport);
        btnReport = (Button) findViewById(R.id.btnReport);
        TextView textLimitLabel = (TextView) findViewById(R.id.textLimitLabel);
        textLimitValue = (TextView) findViewById(R.id.textLimitValue);

        firstName.setTypeface(GlobalConfig.FontType(this, 6));
        emailAddress.setTypeface(GlobalConfig.FontType(this, 6));
        mobileNumber.setTypeface(GlobalConfig.FontType(this, 6));
        messageReport.setTypeface(GlobalConfig.FontType(this, 6));
        btnReport.setTypeface(GlobalConfig.FontType(this, 6));
        textLimitLabel.setTypeface(GlobalConfig.FontType(this, 6));
        textLimitValue.setTypeface(GlobalConfig.FontType(this, 6));

        btnReport.setOnClickListener(this);
        messageReport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int lengthText = 100 - s.length();
                textLimitValue.setText(String.valueOf(lengthText));

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 100)
                    messageReport.setError("Error");
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnReport:
                validateDetails();
                break;
        }
    }

    private void validateDetails() {
        if (!firstName.getText().toString().equals("")) {
            if (!emailAddress.getText().toString().equals("")) {
                if (!mobileNumber.getText().toString().equals("")) {
                    if (!messageReport.getText().toString().equals("")) {

                        String Url = GlobalConfig.BASE_URL + GlobalConfig.SEND_REPORT_METHOD;
                        Map<String, String> requestParams = new HashMap<String, String>();
                        requestParams.put("FirstName", firstName.getText().toString());
                        requestParams.put("MobileNumber", mobileNumber.getText().toString());
                        requestParams.put("EmailAddress", emailAddress.getText().toString());
                        requestParams.put("MessageReport", messageReport.getText().toString());
                        requestParams.put("ReportType", "Self");
                        requestParams.put("ReportAuthorName", "Self");
                        requestParams.put("ReportAuthorId", "");
                        APIManager.stringVolleyRequest("ReportScreen", Url, "POST", requestParams, ReportScreen.this);
                        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
                            @Override
                            public void onSuccessResponse(String response) {
                                Log.e("ReportScreenRes", response);
                                JSONObject responseObject = null;
                                try {
                                    responseObject = new JSONObject(response);
                                    if (responseObject.has("status") && !responseObject.isNull("status") && responseObject.has("message") && !responseObject.isNull("message")) {
                                        String message = responseObject.getString("message");
                                        showAlertDialog(ReportScreen.this, "Report Status", message);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccessJSONResponse(JSONObject response) {

                            }

                            @Override
                            public void onSuccessJSONArrayResponse(JSONArray response) {

                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                APIManager.handleErrorResponseOfRequest(ReportScreen.this, error, "Server Error", "ReportScreen");
                            }

                            @Override
                            public void statusCode(int statusCode) {
                            }
                        });

                    } else {
                        Toast.makeText(ReportScreen.this, "Message Report missing.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ReportScreen.this, "Mobile Number missing.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(ReportScreen.this, "Email Address missing.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ReportScreen.this, "First Name missing.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAlertDialog(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        String positiveText = context.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        ReportScreen.this.finish();
                    }
                });

        String negativeText = context.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        ReportScreen.this.finish();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

}
