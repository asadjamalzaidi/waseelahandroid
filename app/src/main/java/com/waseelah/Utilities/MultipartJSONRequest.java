package com.waseelah.Utilities;

import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by User on 9/27/2016.
 */
public class MultipartJSONRequest extends JsonRequest<JSONObject> {
    private static final String LOG_TAG = MultipartJSONRequest.class.getSimpleName();

    private Map<String, ?> mStringParams;
    private Map<String, ?> mBinaryParams;
    private MultipartEntityBuilder mMultipartEntityBuilder = MultipartEntityBuilder.create();

    private MultipartProgressListener multipartProgressListener = null;
    private long fileLength = 0L;

    public MultipartJSONRequest(int type, String url, Map<String, ?> stringParams, Map<String, ?> binaryParams,
                                Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(type, url, null, listener, errorListener);

        mStringParams = stringParams;
        mBinaryParams = binaryParams;

        Log.v("MULTI ", mStringParams + "");
        Log.v("MULTI B ", mBinaryParams + "");

        buildMultipartEntity();
    }

    public MultipartJSONRequest(int type, String url, Map<String, ?> stringParams, Map<String, ?> binaryParams, long filSize, MultipartProgressListener progressListener,
                                Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(type, url, null, listener, errorListener);

        mStringParams = stringParams;
        mBinaryParams = binaryParams;
        fileLength = filSize;
        multipartProgressListener = progressListener;

        Log.v("MULTI ", mStringParams + "");
        Log.v("MULTI B ", mBinaryParams + "");

        buildMultipartEntity();
    }

    private void buildMultipartEntity() {
        mMultipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        mMultipartEntityBuilder.setBoundary("___________________" + Long.toString(System.currentTimeMillis()));
        mMultipartEntityBuilder.setCharset(Consts.UTF_8);

        for (Map.Entry<String, ?> entry : mStringParams.entrySet())
        {
            try
            {
                if (entry.getValue() instanceof String)
                    mMultipartEntityBuilder.addTextBody(entry.getKey(), (String)entry.getValue());
                else if (entry.getValue() instanceof JSONArray)
                {
                    JSONArray array = (JSONArray)entry.getValue();
                    if(array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            if (array.get(i) instanceof String)
                                mMultipartEntityBuilder.addTextBody(entry.getKey() + "[]", array.getString(i));
                        }
                    }
                    else
                    {
                        mMultipartEntityBuilder.addTextBody(entry.getKey() + "[]", "");
                    }
                }
            }
            catch (Exception e)
            {
                Log.e(LOG_TAG,"buildMultipartEntity Exception");
                e.printStackTrace();
            }
        }

        if (mBinaryParams != null) {
            for (Map.Entry<String, ?> entry : mBinaryParams.entrySet()) {
                ContentType contentType = null;
//                if(mimeType != null && mimeType.length() > 0)
//                    imageContentType = ContentType.create(mimeType);
//                else
//                contentType = ContentType.create("image/jpeg");


                if (entry.getValue() instanceof byte[])
                {
                    contentType = ContentType.create("image/jpeg");
                    Log.d(LOG_TAG, "entry.getValue() => byte[]");
                    mMultipartEntityBuilder.addBinaryBody(entry.getKey(), (byte[]) entry.getValue(), contentType, entry.getKey());
                }
                else if (entry.getValue() instanceof File)
                {
                    Log.d(LOG_TAG, "entry.getValue() => File");
                    File file = (File) entry.getValue();
                    if (file != null) {
                        String mimeType = getMimeType(file.getAbsolutePath());
                        if (mimeType != null && mimeType.length() > 0) {
                            Log.d(LOG_TAG, "entry.getValue() => File Mime Type " + mimeType + " File Name " + file.getName());
                            contentType = ContentType.create(mimeType);
                            // mMultipartEntityBuilder.addBinaryBody(entry.getKey(),(File) entry.getValue());
                            mMultipartEntityBuilder.addBinaryBody(entry.getKey(), file, contentType, file.getName());
                        }
                    }
                }
            /*else if (entry.getValue() instanceof InputStream) {
                Log.d(TAG, "entry.getValue() => InputStream");
                Log.d(TAG, "key: " + entry.getKey());
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) entry.getValue());
                byte[] bitmapByteArray = ImageUtils.getResizedByteArray(bitmap);
                mMultipartEntityBuilder.addBinaryBody("uploadFiles[]", bitmapByteArray, imageContentType, entry.getKey());
            }*/
            }
        }
    }

    @Override
    public String getBodyContentType() {
        return mMultipartEntityBuilder.build().getContentType().getValue();
    }


    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            if (multipartProgressListener == null) {
                mMultipartEntityBuilder.build().writeTo(bos);
            } else {
                mMultipartEntityBuilder.build().writeTo(new CountingOutputStream(bos, fileLength, multipartProgressListener));
            }
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

//    public HttpEntity getEntity() {
//
//        return mMultipartEntityBuilder.build();
//    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        if (response.headers.get("client1") != null)
            Log.e("parseNetworkResponse", "parseNetworkResponse 1 " + response.headers.get("client1"));
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(parsed);
//            if (response.headers.get(GlobalConfig.CLIENT) != null)
//                jsonObject.put(GlobalConfig.CLIENT, response.headers.get(GlobalConfig.CLIENT));
//            if (response.headers.get(GlobalConfig.ACCESS_TOKEN) != null)
//                jsonObject.put(GlobalConfig.ACCESS_TOKEN, response.headers.get(GlobalConfig.ACCESS_TOKEN));
        } catch (JSONException e) {
            jsonObject = null;
        }
        return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));
    }

    public static interface MultipartProgressListener {
        void transferred(long transfered, int progress);
    }

    public static class CountingOutputStream extends FilterOutputStream {
        private final MultipartProgressListener progListener;
        private long transferred;
        private long fileLength;

        public CountingOutputStream(final OutputStream out, long fileLength,
                                    final MultipartProgressListener listener) {
            super(out);
            this.fileLength = fileLength;
            this.progListener = listener;
            this.transferred = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            if (progListener != null) {
                this.transferred += len;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

        public void write(int b) throws IOException {
            out.write(b);
            if (progListener != null) {
                this.transferred++;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

    }

    /**
     * This mtheod is used to get the mime type of file from the  file path
     *
     * @param path - path of file
     * @return - mime type
     */
    public static String getMimeType(String path) {
        try {
            String extention = path.substring(path.lastIndexOf("."));
            String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extention.toLowerCase());
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap);
            return mimeType;
        } catch (Exception e) {
            Log.e(LOG_TAG, "getMimeType Exception");
            e.printStackTrace();
            return "";
        }
    }

}
