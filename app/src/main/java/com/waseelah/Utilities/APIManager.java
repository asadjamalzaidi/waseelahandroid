package com.waseelah.Utilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.waseelah.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by training on 09/08/16.
 */

public class APIManager {
    private static ProgressDialog mProgressDialog;
    public static RequestCallback.APIRequestCallback onAPICallbackListener;

    public static void setOnAPICallbackListener(RequestCallback.APIRequestCallback onAPICallbackListener) {
        APIManager.onAPICallbackListener = onAPICallbackListener;
    }

    public static void jsonObjectVolleyRequest(Context context, String Url, String requestType, JSONObject jsonObject) {
        mProgressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        try {
            int reqType = 0;
            if (requestType.trim().equalsIgnoreCase("GET")) {
                reqType = Request.Method.GET;
            } else if (requestType.trim().equalsIgnoreCase("POST"))
                reqType = Request.Method.POST;

            JsonObjectRequest request = new JsonObjectRequest(reqType, Url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("response>>> ", response + "");
                    if (response.length() > 0) {
                        if (onAPICallbackListener != null)
                            onAPICallbackListener.onSuccessJSONResponse(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.e("response>>> ", error + "");
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.cancel();
                        onAPICallbackListener.onErrorResponse(error);
                    }
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    }
                    return volleyError;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void jsonArrayVolleyRequest(Context context, String Url, String requestType, JSONObject jsonObject) {
        try {

            JsonArrayRequest request = new JsonArrayRequest(Url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    if (response.length() > 0) {
                        if (onAPICallbackListener != null)
                            onAPICallbackListener.onSuccessJSONArrayResponse(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    onAPICallbackListener.onErrorResponse(error);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<>();
                    return params;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                        volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                    }
                    return volleyError;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
 * This method is used to make API call to upload content media for the user
 *
 * @param context - Current Context
 * @param multipartBody - Request body
 */
    public static void makeUploadContentMediaRequest(Context context, MaterialDialog progressDialog,
                                                     HashMap<String, String> params, HashMap<String, String> headerParams,Map<String, ?> fileData,
                                                     Response.Listener successListener, Response.ErrorListener errorListener) {

        try {
            if (context != null && fileData != null && successListener != null && errorListener != null) {

                // Check Internet Connectivity
//                if (GlobalCommonMethods.checkInternetConnection(context)) {

                    String url = GlobalConfig.BASE_URL + GlobalConfig.ALL_USERS + GlobalServiceConfig.ALL_USERS_UPLOAD_MEDIA + "?contenttype=image/png";

                    // Add Custom log to the Fabric Crashlytics
                    if(Answers.getInstance() != null) {
                        Answers.getInstance().logCustom(new CustomEvent("Upload Content Media Request")
                                .putCustomAttribute(GlobalServiceConfig.FCPK_POST, GlobalServiceConfig.ALL_USERS_UPLOAD_MEDIA));
                    }

                    // Make Service request
                    NetworkRequestHandler.getInstance(context).executeMultipartJSONRequest(Request.Method.POST, url, params, headerParams, fileData, successListener, errorListener);

//                } else {
//                    // Show message for no internet
//                    GlobalCommonMethods.showToastMessage(context, context.getResources().getString(R.string.no_internet_found_error_message));
//                    // Dismiss the progress dialog if visible
//                    if (progressDialog != null && progressDialog.isShowing())
//                        progressDialog.dismiss();
//                }
            } else {
                if (context != null)
                    showToastMessage(context, "m");
                    GlobalCommonMethods.showToastMessage(context, GlobalServiceConfig.someThingIsMissingErrorMessage);

                // Dismiss the progress dialog if visible
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "makeAgentPhotoUpdateRequest Exception");
            e.printStackTrace();
            // Show Exception message
            if (e.getMessage() != null)
                GlobalCommonMethods.showToastMessage(context, e.getMessage());

            // Dismiss the progress dialog if visible
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

////////////////////////////////////////

    /**
     * This method is used to add the new json request to queue.
     *
     * @param method          - Type of request
     * @param url             - url of request
     * @param params          - parameters
     * @param headerParams    - header parameters
     * @param successListener - success listener
     * @param errorListener   - error listener
     */
    public void executeMultipartJSONRequest(Context context, int method, String url, final HashMap<String, String> params, final HashMap<String, String> headerParams,
                                            Map<String, ?> fileData,
                                            Response.Listener successListener,
                                            Response.ErrorListener errorListener) {

        MultipartJSONRequest request = new MultipartJSONRequest(method, url, params, fileData,
                successListener, errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (headerParams != null)
                    return headerParams;
                else
                    return super.getHeaders();
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if (response.headers.get("client1") != null)
                    Log.e("parseNetworkResponse", "parseNetworkResponse 1 " + response.headers.get("client1"));
                String parsed;
                try {
                    parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                } catch (UnsupportedEncodingException e) {
                    parsed = new String(response.data);
                }
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(parsed);
//                    if (response.headers.get(GlobalConfig.CLIENT) != null)
//                        jsonObject.put(GlobalConfig.CLIENT, response.headers.get(GlobalConfig.CLIENT));
//                    if (response.headers.get(GlobalConfig.ACCESS_TOKEN) != null)
//                        jsonObject.put(GlobalConfig.ACCESS_TOKEN, response.headers.get(GlobalConfig.ACCESS_TOKEN));
                } catch (JSONException e) {
                    jsonObject = null;
                }
                return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));

//                return super.parseNetworkResponse(response);
            }

//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError) {
//                Log.e("Volly Error","Volley Error " + volleyError.getMessage());
//
//                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
//                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
//                    volleyError = error;
//                }
//
//                return volleyError;
////                return super.parseNetworkError(volleyError);
//            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(6000000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }

    public static void stringVolleyRequest(final String TAG, String Url, String requestType, final Map<String, String> requestParams, Context context) {

        mProgressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        mProgressDialog.setMessage("Validating...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        int reqType = 0;
        if (requestType.trim().equalsIgnoreCase("GET")) {
            reqType = Request.Method.GET;
        } else if (requestType.trim().equalsIgnoreCase("POST"))
            reqType = Request.Method.POST;

        StringRequest jsonObjRequest = new StringRequest(reqType, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.cancel();
                        if (!response.equals("")) {
                            if (onAPICallbackListener != null)
                                onAPICallbackListener.onSuccessResponse(response);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.cancel();
                    onAPICallbackListener.onErrorResponse(error);

                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return requestParams;
            }

        };
        jsonObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjRequest);
    }

    /**
     * This method is used to handle the error response of network request and show appropriate message.
     *
     * @param context      - Current context
     * @param error        - Volley error in response
     * @param errorMessage - default error message
     */
    public static void handleErrorResponseOfRequest(Context context, VolleyError error, String errorMessage, String logTitle) {
        if (context != null && error != null && errorMessage != null && errorMessage.length() > 0 && logTitle != null) {
            try {
                if (error.networkResponse != null) {
                    if (error.networkResponse.data != null) {
                        String responseString = new String(error.networkResponse.data);
                        JSONObject responseJson = new JSONObject(responseString);
                        if (responseJson.optJSONObject("error") != null) {
                            JSONObject errorsJSON = responseJson.getJSONObject("error");
                            if (errorsJSON != null) {
                                if ((errorsJSON.optString("name", "")).equalsIgnoreCase("Blocked")) {
                                    String message = errorsJSON.optString("message", "") + ". Please report if you want yourself to be unblocked";
                                    showAlertDialog(context, "Report", message);
                                } else {
                                    showToastMessage(context, errorsJSON.optString("message", ""));
                                }
                            } else {
                                showToastMessage(context, errorMessage);
                            }
                        } else {
                            showToastMessage(context, errorMessage);
                        }
                    } else {
                        // Show appropriate error message
                        if (error.getMessage() != null) {
                            showToastMessage(context, error.getMessage());
                        } else {
                            showToastMessage(context, errorMessage);
                        }
                    }
                } else {
                    // Error Network response is null so show appropriate error message
                    if (error.getMessage() != null) {
                        showToastMessage(context, error.getMessage());
                    } else {
                        showToastMessage(context, errorMessage);
                    }
                }
            } catch (JSONException e) {
                Log.e(logTitle, " Exception");
                e.printStackTrace();
                // Show appropriate error message
                if (error.getMessage() != null) {
                    showToastMessage(context, error.getMessage());
                } else {
                    showToastMessage(context, errorMessage);
                }
            } catch (Exception e) {
                Log.e(logTitle, " Exception");
                e.printStackTrace();
                // Show appropriate error message
                showToastMessage(context, errorMessage);
            }
        }

    }

    private static void showToastMessage(Context context, String errorMsg) {
        Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
    }

    private static void showAlertDialog(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        String positiveText = context.getString(R.string.report);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        Intent n = new Intent(context, ReportScreen.class);
                        n.putExtra("EmailAddress", "");
                        n.putExtra("MobileNumber", "");
                        n.putExtra("FirstName", "");
                        context.startActivity(n);
                    }
                });

        String negativeText = context.getString(R.string.ignore);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic

                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

}
