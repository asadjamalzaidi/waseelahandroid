package com.waseelah.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by training on 09/08/16.
 */

public class GlobalConfig {
    private static String PREF_NAME = "WaseelahPref";

    public static String BASE_URL = "http://54.224.154.74:3000/api";
    public static String MEDIA_URL = "";
//        public static String BASE_URL = "http://waseelah.herokuapp.com/api";
//    public static String BASE_URL = "http://192.168.1.4:3001/api";
//    public static String BASE_URL = "http://10.0.0.143:3001/api";
    public static String AUTHENTICATE_METHOD = "/userauths/authenticate";
    public static String REGISTER_METHOD = "/userauths/register";
    public static String SEND_REPORT_METHOD = "/reportlogs/sendReport";
    public static String VERIFY_MOBILE_METHOD = "/userauths/verifyMobileNumber";
    public static String GET_ALL_POST = "/posts/getAllPost";
//  ========================ANJUMAN=========================
    public static String CREATE_ANJUMAN = "/anjumans/createAnjuman";
    public static String GET_ALL_ANJUMAN_FOR_OFFICIALS = "/anjumans/getAllAnjumanForOfficials";
    public static String GET_ALL_ANJUMAN = "/anjumans/getAllAnjuman";
    public static String FOLLOW_ANJUMAN = "/anjumans/followAnjuman";

    public final static int DASHBOARD = 0, ANJUMAN = 1, SOCIETY = 2, MASJID = 3, SETTINGS = 4, LOGOUT = 5;
    public static Typeface FontType(Context context, int id) {
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/Montserrat-Regular.otf");
        switch (id) {
            case 1:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-Black.otf");
                break;
            case 2:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-Bold.otf");
                break;
            case 3:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-ExtraBold.otf");
                break;
            case 4:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-Hairline.otf");
                break;
            case 5:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-Light.otf");
                break;
            case 6:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-Regular.otf");
                break;
            case 7:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-SemiBold.otf");
                break;
            case 8:
                face = Typeface.createFromAsset(context.getAssets(),
                        "fonts/Montserrat-UltraLight.otf");
                break;
        }
        return face;
    }

    public static String encodePassword(String source) {
        String value = "";
        byte[] byteArray;
        try {
            byteArray = source.getBytes("ASCII");
            value = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            Log.e("value", value);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return value;
    }

    public static void setSharedValuePreference(Context context, String key, String value) {
        SharedPreferences shared = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getSharedValuePreference(Context context, String key){
        SharedPreferences shared = context.getSharedPreferences(PREF_NAME,  Context.MODE_PRIVATE);
        return (shared.getString(key, ""));
    }

}
