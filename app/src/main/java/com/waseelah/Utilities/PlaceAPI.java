package com.waseelah.Utilities;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.waseelah.Model.PlacesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by training on 08/08/16.
 */

public class PlaceAPI {

    private static final String TAG = PlaceAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAILS = "/details";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyBMhM-JEz20Gx9EBCbhmY2Unk-gqBVZ74Q";
    private boolean cityRequested = false;
    public ArrayList<PlacesModel> autocomplete(String stats, LatLng latLong, String input) throws IOException {
        ArrayList<PlacesModel> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            if (stats.equals("Location")) {
                sb.append("&location=" + latLong.latitude + "," + latLong.longitude);
                cityRequested = false;
            }
            else if (stats.equals("Venue")){
                sb.append("&types=geocode");
                cityRequested = false;
            }  else {
                sb.append("&types=(cities)");
                cityRequested = true;
            }
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            Log.i("URL: ", sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<PlacesModel>(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                boolean invalidCity =false;
                PlacesModel placesModel = new PlacesModel();
                if (predsJsonArray.getJSONObject(i).has("description") && !predsJsonArray.getJSONObject(i).isNull("description")) {
                    placesModel.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                } else {
                    placesModel.setDescription("");
                }

                if (predsJsonArray.getJSONObject(i).has("place_id") && !predsJsonArray.getJSONObject(i).isNull("place_id")) {
                    placesModel.setPlacesId(predsJsonArray.getJSONObject(i).getString("place_id"));
                } else {
                    placesModel.setPlacesId("");
                }

                if (predsJsonArray.getJSONObject(i).has("terms") && !predsJsonArray.getJSONObject(i).isNull("terms")) {
                    JSONArray jsonArray = predsJsonArray.getJSONObject(i).getJSONArray("terms");
                    if (cityRequested && jsonArray.length() == 3) {
                        if (jsonArray.getJSONObject(0).has("value") && jsonArray.getJSONObject(0).has("value") && !jsonArray.getJSONObject(0).isNull("value")) {
                            placesModel.setCity(jsonArray.getJSONObject(0).getString("value"));
                        } else {
                            placesModel.setCity("");
                        }
                        if (jsonArray.getJSONObject(1).has("value") && !jsonArray.getJSONObject(1).isNull("value")) {
                            placesModel.setState(jsonArray.getJSONObject(1).getString("value"));
                        } else {
                            placesModel.setState("");
                        }
                        if (jsonArray.getJSONObject(2).has("value") && !jsonArray.getJSONObject(2).isNull("value")) {
                            placesModel.setCountry(jsonArray.getJSONObject(2).getString("value"));
                        } else {
                            placesModel.setCountry("");
                        }
                    }
                    else{
                        if (jsonArray.length() > 3) {
                            int length = jsonArray.length();
                            if (jsonArray.getJSONObject(length - 1).has("value") && !jsonArray.getJSONObject(length - 1).isNull("value")) {
                                placesModel.setCountry(jsonArray.getJSONObject(length - 1).getString("value"));
                            } else {
                                placesModel.setCountry("");
                            }
                            if (jsonArray.getJSONObject(length - 2).has("value") && !jsonArray.getJSONObject(length - 2).isNull("value")) {
                                placesModel.setState(jsonArray.getJSONObject(length - 2).getString("value"));
                            } else {
                                placesModel.setState("");
                            }
                            if (jsonArray.getJSONObject(length - 3).has("value") && !jsonArray.getJSONObject(length - 3).isNull("value")) {
                                placesModel.setCity(jsonArray.getJSONObject(length - 3).getString("value"));
                            } else {
                                placesModel.setCity("");
                            }

                            int calculatedLength = length - 3;
                            String desiredLocationFullName = "";
                            for (int j = 0; j < calculatedLength; j++) {
                                if (jsonArray.getJSONObject(j).has("value") && !jsonArray.getJSONObject(j).isNull("value")) {
                                    if (j == calculatedLength - 1) {
                                        desiredLocationFullName = desiredLocationFullName + jsonArray.getJSONObject(j).getString("value");
                                    } else {
                                        desiredLocationFullName = desiredLocationFullName + jsonArray.getJSONObject(j).getString("value") + ",";
                                    }
                                } else {
                                    desiredLocationFullName = desiredLocationFullName + "";
                                }
                            }
                            placesModel.setDesiredLocation(desiredLocationFullName);

                        } else {
                            invalidCity = true;
                            placesModel.setCity("");
                            placesModel.setState("");
                            placesModel.setCountry("");
                            placesModel.setDesiredLocation("");
                        }
                    }

                } else {
                    placesModel.setCity("");
                    placesModel.setState("");
                    placesModel.setCountry("");
                    placesModel.setDesiredLocation("");
                }
                if (!invalidCity){
                    resultList.add(placesModel);
                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

}
