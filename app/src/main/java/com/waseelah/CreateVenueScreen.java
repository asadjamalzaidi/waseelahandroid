package com.waseelah;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AddPlaceRequest;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.MapActivity;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 9/3/2016.
 */
public class CreateVenueScreen extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private String TAG = "CreateVenue";
    private GoogleApiClient mGoogleApiClient;
    private Context context;
    private View toolbarView;
    private int PLACE_PICKER_REQUEST = 1;
    private EditText place_name,place_address, place_phone_number, place_website;
    private RelativeLayout submit_layout, place_picker;
    private TextView submit,pick_your_place;
    private LatLng pickedLatLng;
    private boolean placePicked = false;
    private String latitude ="", longitude="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_venue_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = CreateVenueScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView textTopLeft = (TextView) toolbarView.findViewById(R.id.textTopLeft);
        textTopLeft.setVisibility(View.VISIBLE);
        textTopLeft.setText("Create Venue");

        place_name = (EditText) findViewById(R.id.place_name);
        place_address = (EditText) findViewById(R.id.place_address);
        place_phone_number = (EditText) findViewById(R.id.place_phone_number);
        place_website = (EditText) findViewById(R.id.place_website);
        submit_layout = (RelativeLayout) findViewById(R.id.submit_layout);
        place_picker = (RelativeLayout) findViewById(R.id.place_picker);
        submit = (TextView) findViewById(R.id.submit);
        pick_your_place = (TextView)findViewById(R.id.pick_your_place);

        place_name.setTypeface(GlobalConfig.FontType(this, 6));
        place_address.setTypeface(GlobalConfig.FontType(this, 6));
        place_phone_number.setTypeface(GlobalConfig.FontType(this, 6));
        place_website.setTypeface(GlobalConfig.FontType(this, 6));
        submit.setTypeface(GlobalConfig.FontType(this, 6));

        place_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//
//                try {
//                    startActivityForResult(builder.build(CreateVenueScreen.this), PLACE_PICKER_REQUEST);
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
                Intent nextScreen = new Intent(CreateVenueScreen.this,
                        MapActivity.class);
                startActivityForResult(nextScreen, 123);
            }
        });



        submit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean validateStatus = validateDetails();
                if (validateStatus){
                    String name = place_name.getText().toString();
                    String address = place_address.getText().toString();
                    String phone_number = place_phone_number.getText().toString();
                    String website = place_website.getText().toString();
//                    double lat = pickedLatLng.latitude;
//                    double lng = pickedLatLng.longitude;
                    double lat = Double.parseDouble(latitude);
                    double lng = Double.parseDouble(longitude);
                    addPlacesToGoogleDB(name, address, phone_number, website, lat, lng);
                }
            }
        });

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private boolean validateDetails() {
        if (place_name.getText().toString().trim().equals("")) {
            showToastMessage("Place Name cannot be blank");
            return false;
        } else if (place_address.getText().toString().trim().equals("")) {
            showToastMessage("Place Address cannot be blank");
            return false;
        } else if (!placePicked) {
            showToastMessage("Place select a place");
            return false;
        } else if (place_phone_number.getText().toString().trim().equals("")) {
            showToastMessage("Phone Number cannot be blank");
            return false;
        } else {
            return true;
        }
    }

    private void showToastMessage(String message) {
        Toast.makeText(CreateVenueScreen.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case 123:
                        if (data.hasExtra("Latitude")) {
                            latitude = data.getStringExtra("Latitude");
                        }
                        if (data.hasExtra("Longitude")) {
                            longitude = data.getStringExtra("Longitude");
                        }
                        String latLng = "Lng: " + longitude + ", Lat: " + latitude;
                        pick_your_place.setText(latLng);
                        if (!placePicked){
                            placePicked= true;
                        }
                        break;
//                Place place = PlacePicker.getPlace(this, data);
//                String toastMsg = String.format("Place: %s", place.getName());
//                pickedLatLng = place.getLatLng();
//                if (!placePicked){
//                    placePicked = true;
//                }
//                String latLng = "Lng: " + pickedLatLng.longitude + ", Lat: " + pickedLatLng.latitude;
//                pick_your_place.setText(latLng);
//                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                }
//            }
//        }
    }

    private void addPlacesToGoogleDB(String placeName, String Address, String PhoneNumber,
                                     String websiteName, double latitude, double longitude) {
        AddPlaceRequest place =
                new AddPlaceRequest(
                        placeName, // Name
                        new LatLng(latitude, longitude), // Latitude and longitude
                        Address, // Address
                        Collections.singletonList(Place.TYPE_OTHER), // Place types
                        PhoneNumber, // Phone number
                        Uri.parse(websiteName) // Website
                );

        Places.GeoDataApi.addPlace(mGoogleApiClient, place)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {

                        Log.i(TAG, "Place add result: " + places.getStatus().toString());
                        Log.i(TAG, "Added place: " + places.get(0).getName().toString());
                        Log.i(TAG, "Added place: " + places.get(0).getId());
                        Log.i(TAG, "Added place: " + places.get(0).getLatLng());
                        Log.i(TAG, "Added place: " + places.get(0));



                        places.release();
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
