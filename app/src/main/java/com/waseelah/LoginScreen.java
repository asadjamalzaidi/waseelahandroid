package com.waseelah;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.waseelah.DesiredLocation.DesiredContainer;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.ReportScreen;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {

    private EditText mobileNumber, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        password = (EditText) findViewById(R.id.password);
        Button logInButton = (Button) findViewById(R.id.logInButton);
        Button btnRegister = (Button) findViewById(R.id.registerButton);
        mobileNumber.setTypeface(GlobalConfig.FontType(this, 6));
        password.setTypeface(GlobalConfig.FontType(this, 6));
        logInButton.setTypeface(GlobalConfig.FontType(this, 6));
        btnRegister.setTypeface(GlobalConfig.FontType(this, 6));
        logInButton.setOnClickListener(this);
        btnRegister.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logInButton:
                if (mobileNumber != null && mobileNumber.hasFocus()) {
                    ((InputMethodManager) LoginScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mobileNumber.getWindowToken(), 0);
                }
                if (password != null && password.hasFocus()) {
                    ((InputMethodManager) LoginScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(password.getWindowToken(), 0);
                }
                validateLogin();
                break;

            case R.id.registerButton:
                Intent n = new Intent(LoginScreen.this, RegisterScreen.class);
                startActivity(n);
                break;
        }
    }

    private void validateLogin() {
        if (!mobileNumber.getText().toString().equals("")) {
            if (!password.getText().toString().equals("")) {
                String Url = GlobalConfig.BASE_URL + GlobalConfig.AUTHENTICATE_METHOD;
                String passwordCalculated = GlobalConfig.encodePassword(password.getText().toString());
                Map<String, String> requestParams = new HashMap<String, String>();
                requestParams.put("MobileNumber", mobileNumber.getText().toString());
                requestParams.put("Password", passwordCalculated);
                APIManager.stringVolleyRequest("LoginScreen", Url, "POST", requestParams, LoginScreen.this);
                APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        Log.e("response", response);
                        goToDesiredLocationContainer(response);
                    }

                    @Override
                    public void onSuccessJSONResponse(JSONObject response) {

                    }

                    @Override
                    public void onSuccessJSONArrayResponse(JSONArray response) {

                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        APIManager.handleErrorResponseOfRequest(LoginScreen.this, error, "Server Error", "LoginScreen");
                    }

                    @Override
                    public void statusCode(int statusCode) {
                    }
                });
            } else {
                Toast.makeText(LoginScreen.this, "Password missing.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(LoginScreen.this, "Mobile Number missing.", Toast.LENGTH_SHORT).show();
        }
    }

    private void goToDesiredLocationContainer(String response) {
        String blank = "";

        try {
            JSONObject jsonResponse = new JSONObject(response);
            if (jsonResponse.has("MobileNumber") && !jsonResponse.isNull("MobileNumber")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "MobileNumber", jsonResponse.getString("MobileNumber"));
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "MobileNumber", blank);
            }
            if (jsonResponse.has("UserId") && !jsonResponse.isNull("UserId")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "UserId", jsonResponse.getInt("UserId")+"");
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "UserId", blank);
            }
            if (jsonResponse.has("FirstName") && !jsonResponse.isNull("FirstName")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "FirstName", jsonResponse.getString("FirstName"));
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "FirstName", blank);
            }
            if (jsonResponse.has("AccessToken") && !jsonResponse.isNull("AccessToken")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "AccessToken", jsonResponse.getString("AccessToken"));
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "AccessToken", blank);
            }
            if (jsonResponse.has("LastLogin") && !jsonResponse.isNull("LastLogin")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "LastLogin", jsonResponse.getString("LastLogin"));
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "LastLogin", blank);
            }
            if (jsonResponse.has("DesiredLocationId") && !jsonResponse.isNull("DesiredLocationId")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "DesiredLocationId", jsonResponse.getInt("DesiredLocationId")+"");
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "DesiredLocationId", blank);
            }
            if (jsonResponse.has("DesiredLocation") && !jsonResponse.isNull("DesiredLocation")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "DesiredLocation", jsonResponse.getString("DesiredLocation"));
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "DesiredLocation", blank);
            }
            if (jsonResponse.has("isMobileVerified") && !jsonResponse.isNull("isMobileVerified")) {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "isMobileVerified", jsonResponse.getBoolean("isMobileVerified")+"");
            } else {
                GlobalConfig.setSharedValuePreference(LoginScreen.this, "isMobileVerified", blank);
            }
            Intent n = new Intent(LoginScreen.this, DesiredContainer.class);
            startActivity(n);
            finish();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
