package com.waseelah;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.waseelah.Adapter.GalleryGridAdapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by User on 8/28/2016.
 */
public class SelectImageScreen extends AppCompatActivity {
    private Context context;
    private View toolbarView;
    private Cursor cursor;
    private ProgressDialog myProgressDialog;
    private GridView gridview;
    private static Uri[] mUrls = null;
    private static String[] strUrls = null;
    private String[] mNames = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_image_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = SelectImageScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        ImageView cameraCapture = (ImageView) toolbarView.findViewById(R.id.cameraCapture);
        cameraCapture.setVisibility(View.VISIBLE);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent previousScreen = new Intent(getApplicationContext(), ContentVisualizingScreen.class);
                setResult(999, previousScreen);
                finish();
            }
        });
        TextView textTopLeft = (TextView) toolbarView.findViewById(R.id.textTopLeft);
        textTopLeft.setVisibility(View.VISIBLE);
        textTopLeft.setText("Gallery");
        gridview = (GridView) findViewById(R.id.gridViewGallery);
//        cursor = this.getContentResolver().query(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null,
//                MediaStore.Images.Media.DATE_ADDED + " DESC");

//        if (cursor != null) {
//            myProgressDialog = new ProgressDialog(SelectImageScreen.this);
//            myProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            myProgressDialog.setMessage("Buffering");
//            myProgressDialog.show();
//            gridview = (GridView) findViewById(R.id.gridViewGallery);
//            new Thread() {
//                public void run() {
//                    try {
//                        cursor.moveToFirst();
//                        mUrls = new Uri[cursor.getCount()];
//                        strUrls = new String[cursor.getCount()];
//                        mNames = new String[cursor.getCount()];
//                        for (int i = 0; i < cursor.getCount(); i++) {
//                            cursor.moveToPosition(i);
//                            mUrls[i] = Uri.parse(cursor.getString(1));
//                            strUrls[i] = cursor.getString(1);
//                            mNames[i] = cursor.getString(3);
//                        }
//                    } catch (Exception e) {
//                    }
//                    myProgressDialog.dismiss();
//                }
//            }.start();

            final ArrayList<String> imagePath = getAllShownImagesPath();
            gridview.setAdapter(new GalleryGridAdapter(SelectImageScreen.this, cursor, imagePath));
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {

                    Intent previousScreen = new Intent(getApplicationContext(), ContentVisualizingScreen.class);
                    previousScreen.putExtra("ImageURI",imagePath.get(position));
                    setResult(1000, previousScreen);
                    finish();
                }
            });
//        }

        cameraCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
            }
        });
    }
    public ArrayList<String> getAllShownImagesPath() {
        Uri uri;
        Cursor cursor;
        int column_index;
        StringTokenizer st1;
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA };

        cursor = SelectImageScreen.this.getContentResolver().query(uri, projection, null,
                null, MediaStore.Images.Media.DATE_ADDED + " DESC");

        // column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index);
            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
//                    Uri selectedImage = imageReturnedIntent.getData();
                    Intent previousScreen = new Intent(getApplicationContext(), ContentVisualizingScreen.class);
                    if(tempUri!=null){
                        previousScreen.putExtra("CameraImageURI",tempUri.toString());
                    }
                    else{
                        previousScreen.putExtra("Error","No image found");
                    }
                    setResult(1000, previousScreen);
                    finish();

                }
                break;
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
