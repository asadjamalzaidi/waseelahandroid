package com.waseelah;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 8/15/2016.
 */
public class VerifyMobileScreen extends AppCompatActivity {
    private  EditText verificationCode;
    private String mobileNumber;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verifymobile_screen);
        Intent intent = getIntent();
        mobileNumber= intent.getStringExtra("MobileNumber");
        verificationCode = (EditText)findViewById(R.id.verificationCode);
        Button verifyButton = (Button)findViewById(R.id.verifyButton);
        verifyButton.setTypeface(GlobalConfig.FontType(this, 6));
        verificationCode.setTypeface(GlobalConfig.FontType(this, 6));
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!verificationCode.getText().toString().equals("")){
                    sendVerificationCode();
                }else{
                    Toast.makeText(VerifyMobileScreen.this, "Verification code cannot be blank.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void sendVerificationCode(){
        String Url = GlobalConfig.BASE_URL + GlobalConfig.VERIFY_MOBILE_METHOD;
        Map<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("MobileNumber", mobileNumber);
        requestParams.put("VerificationCode", verificationCode.getText().toString());
        APIManager.stringVolleyRequest("LoginScreen", Url, "POST", requestParams, VerifyMobileScreen.this);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {
                Log.e("response", response);
                Toast.makeText(VerifyMobileScreen.this,"Verified",Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {

            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                APIManager.handleErrorResponseOfRequest(VerifyMobileScreen.this, error, "Server Error", "VerifyMobileScreen");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });
    }
}
