package com.waseelah.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.waseelah.Model.PostModel;
import com.waseelah.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 8/28/2016.
 */
public class HomeScreenRecyclerAdapter extends RecyclerView.Adapter<HomeScreenRecyclerAdapter.MyViewHolder> {

    private ArrayList<PostModel> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView postedBy, postedTime, postedLocation, likeCount, commentCount, likeButton, commentButton, shareButton;
        CircleImageView circleImageView;
        ImageView postedImage;

        public MyViewHolder(View view) {
            super(view);
            postedBy = (TextView) view.findViewById(R.id.postedBy);
            postedTime = (TextView) view.findViewById(R.id.postedDesiredLocation);
            postedLocation = (TextView) view.findViewById(R.id.postedTime);
            likeCount = (TextView) view.findViewById(R.id.textCount);
            commentCount = (TextView) view.findViewById(R.id.textComments);
            likeButton = (TextView) view.findViewById(R.id.likeButton);
            commentButton = (TextView) view.findViewById(R.id.commentButton);
            shareButton = (TextView) view.findViewById(R.id.shareButton);
            circleImageView = (CircleImageView)view.findViewById(R.id.circleImageView);
            postedImage = (ImageView)view.findViewById(R.id.postedImage);
        }
    }


    public HomeScreenRecyclerAdapter(Context context, ArrayList<PostModel> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.postedBy.setText(arrayList.get(position).getPostedBy());
        holder.postedTime.setText(arrayList.get(position).getPostedAt());
        holder.postedLocation.setText(arrayList.get(position).getDesiredLocation());
        String likeCount = arrayList.get(position).getLikeCount() + " Likes";
        String commentCount = arrayList.get(position).getCommentCount() + " Comments";
        holder.likeCount.setText(likeCount);
        holder.commentCount.setText(commentCount);
        Glide.with(context)
                .load("https://dl.dropboxusercontent.com/apitl/1/AAD4408VKHkdR8b9g3XEI3vmfnWrG8x1g41j5kVVPwicPdUspQwpnwJxHoyLVziyGsHfVToNNGy1dhYti_BCQ4IX6P3W1Z-JNzKS96atpR25shQugbKJ-GyyFigq5UVtCMzlHNzu2-ARGzgoycQU8NKUirK6kHc-d4NhJ3Wm5vU9Bl2-7YvW0eYOY8_5TfdrcAKFuhTxbyTKfWIEHFc6d3-G4qQPDRXYhOjwevHzBnIZCA")
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.circleImageView);
        Glide.with(context)
                .load(arrayList.get(position).getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.postedImage);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}