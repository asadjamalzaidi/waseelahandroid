package com.waseelah.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.waseelah.Model.PlacesModel;
import com.waseelah.Utilities.PlaceAPI;
import com.waseelah.R;
import com.waseelah.Utilities.GlobalConfig;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by training on 08/08/16.
 */

public class PlacesAutoCompleteAdapter extends ArrayAdapter<PlacesModel> implements Filterable {

    private ArrayList<PlacesModel> resultList;
    private Context mContext;
    private int mResource;
    private PlaceAPI mPlaceAPI = new PlaceAPI();
    private String stats;
    private LatLng latLng;
    public PlacesAutoCompleteAdapter(String stats, LatLng latLng, Context context, int resource) {
        super(context, resource);
        this.stats = stats;
        mContext = context;
        mResource = resource;
        this.latLng = latLng;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        PlacesModel place = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.autocomplete_list_item, parent, false);
        }
        // Lookup view for data population
        TextView cityName = (TextView) convertView.findViewById(R.id.autocompleteText);
        cityName.setTypeface(GlobalConfig.FontType(mContext, 6));
        cityName.setText(place.getDescription());
        return convertView;
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public PlacesModel getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        if (stats.equals("Location")){
                            resultList = mPlaceAPI.autocomplete(stats, latLng ,constraint.toString());
                        }
                        else{
                            resultList = mPlaceAPI.autocomplete(stats, null, constraint.toString());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

}
