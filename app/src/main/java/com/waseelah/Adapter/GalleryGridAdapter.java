package com.waseelah.Adapter;

/**
 * Created by User on 8/28/2016.
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.waseelah.R;

import java.io.File;
import java.util.ArrayList;

public class GalleryGridAdapter extends BaseAdapter {
    private ArrayList<String> mUrls;
    private Context mContext;
    private Cursor cc;
    private LayoutInflater mLayoutInflater;

    public GalleryGridAdapter(Context c, Cursor cc, ArrayList<String> mUrls) {
        mLayoutInflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;
        this.cc = cc;
        this.mUrls = mUrls;
    }

    public int getCount() {
        return mUrls.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mVHolder;
        if(convertView == null){
            convertView=mLayoutInflater.inflate(R.layout.grid_view_items, parent, false);
            mVHolder=new ViewHolder();

            mVHolder.mImageView=(ImageView)convertView.findViewById(R.id.image_grid);
            convertView.setTag(mVHolder);
        }else{
            mVHolder=(ViewHolder)convertView.getTag();
        }
        Glide.with(mContext)
                    .load(new File(Uri.parse(mUrls.get(position)).getPath()))
                    .diskCacheStrategy( DiskCacheStrategy.RESULT )
                    .into(mVHolder.mImageView);

        return convertView;
    }
    private static class ViewHolder{
        ImageView mImageView;
    }
}
