package com.waseelah.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.waseelah.Model.AnjumanListModel;
import com.waseelah.R;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 9/11/2016.
 */
public class AnjumanListRecyclerAdapter extends RecyclerView.Adapter<AnjumanListRecyclerAdapter.MyViewHolder> {
    private ProgressDialog mProgressDialog;
    private ArrayList<AnjumanListModel> arrayList;
    private Context context;
    private MyViewHolder myHolder;
    int myPosition;
    private boolean following = false;
    private String followText = "Joined";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textFollowerCount, textAnjumanName, follow_text;
        RelativeLayout followRelativeLayout;
        CircleImageView anjumanProfileImage;
        ImageView coverImage;

        public MyViewHolder(View view) {
            super(view);
            textFollowerCount = (TextView) view.findViewById(R.id.textFollowerCount);
            textAnjumanName = (TextView) view.findViewById(R.id.textAnjumanName);
            follow_text = (TextView) view.findViewById(R.id.follow_text);
            followRelativeLayout = (RelativeLayout) view.findViewById(R.id.followRelativeLayout);
            anjumanProfileImage = (CircleImageView) view.findViewById(R.id.anjumanProfileImage);
            coverImage = (ImageView) view.findViewById(R.id.coverImage);
            textFollowerCount.setTypeface(GlobalConfig.FontType(context, 6));
            textAnjumanName.setTypeface(GlobalConfig.FontType(context, 6));
            follow_text.setTypeface(GlobalConfig.FontType(context, 6));
        }
    }


    public AnjumanListRecyclerAdapter(Context context, ArrayList<AnjumanListModel> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.anjuman_recycler_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        myHolder = holder;
        myPosition = position;
        int userId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "UserId"));
        ArrayList aList = new ArrayList(Arrays.asList(arrayList.get(position).getFollowersId().split(",")));
        if (arrayList.get(position).getUserId() == userId) {
            holder.followRelativeLayout.setVisibility(View.GONE);
        }
        if (aList.contains(userId + "")) {
            if (holder.followRelativeLayout.getVisibility() == View.VISIBLE) {
                holder.followRelativeLayout.setVisibility(View.VISIBLE);
            }

            holder.follow_text.setText(followText);
            following = true;
        }
        holder.textFollowerCount.setText("Followers: " + arrayList.get(position).getFollowers());
        holder.textAnjumanName.setText(arrayList.get(position).getName());
        Glide.with(context)
                .load(arrayList.get(position).getProfileImage())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.anjumanProfileImage);
        Glide.with(context)
                .load(arrayList.get(position).getCoverImage())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.coverImage);

        holder.followRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!following) {
                    myHolder.follow_text.setText(followText);
                    following = true;
                    followAnjuman(arrayList.get(myPosition).getUuid());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private void followAnjuman(String uuid) {
        int DesiredLocationId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "DesiredLocationId"));
        int UserId = Integer.parseInt(GlobalConfig.getSharedValuePreference(context, "UserId"));

        String Url = GlobalConfig.BASE_URL + GlobalConfig.FOLLOW_ANJUMAN;
        JSONObject obj = new JSONObject();
        try {
            obj.put("AnjumanUUID", uuid);
            obj.put("DesiredLocationId", DesiredLocationId);
            obj.put("UserId", UserId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url = Url + "?AnjumanUUID=" + uuid + "&DesiredLocationId=" + DesiredLocationId + "&UserId=" + UserId;
        APIManager.jsonObjectVolleyRequest(context, Url, "POST", obj);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {

            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {
                Log.e("response", response + "");
            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                APIManager.handleErrorResponseOfRequest(context, error, "Server Error", "CreateAnjumanScreen");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });

    }
}
