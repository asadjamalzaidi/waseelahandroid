package com.waseelah;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter.CalendarDay;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.waseelah.Adapter.PlacesAutoCompleteAdapter;
import com.waseelah.Model.PlacesModel;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.MapActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 8/28/2016.
 */
public class ContentVisualizingScreen extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private Context context;
    private String LOG_TAG = "Content Uplaod";
    private View toolbarView;
    private final int RESULT_CROP = 400;
    private Uri mCropImagedUri;
    private EditText speakerName, topic, organiser, soz_salam, shayar, nauhakhwani,
            organiser_address, organiser_contact, organiser_email, organiser_website;
    private TextView date_value, time_value, add_post;
    private AutoCompleteTextView venue;
    private RelativeLayout addPostLayout;
    private CircleImageView contentImage;
    private GoogleApiClient mGoogleApiClient;
    private String venueCityName, venueStateName, venueCountryName, venueLocationName,
            venueLocationPlaceId;
    private Double venueLatitude = 0.0, venueLongitude = 0.0;
    private ProgressDialog mProgressDialog;
    private ImageView testing;
    private String dateString, timeString;
    private Bitmap contentImageBitmap;
    private int recursiveLogicCounter = 0;
    private int positionBreakPoint = 0;
    private String globalDate, globalTime;
    private String unixTimeStamp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_visualizing_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbarView = getLayoutInflater().inflate(R.layout.toolbar_view, toolbar, false);
        toolbar.addView(toolbarView);
        context = ContentVisualizingScreen.this;
        ImageView leftArrow = (ImageView) toolbarView.findViewById(R.id.leftArrow);
        leftArrow.setVisibility(View.VISIBLE);
        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView textTopLeft = (TextView) toolbarView.findViewById(R.id.textTopLeft);
        textTopLeft.setVisibility(View.VISIBLE);
        textTopLeft.setText("Compose");

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .build();

        speakerName = (EditText) findViewById(R.id.speakerName);
        topic = (EditText) findViewById(R.id.topic);
        venue = (AutoCompleteTextView) findViewById(R.id.venue);
        soz_salam = (EditText) findViewById(R.id.soz_salam);
        shayar = (EditText) findViewById(R.id.shayar);
        nauhakhwani = (EditText) findViewById(R.id.nauhakhwani);
        organiser = (EditText) findViewById(R.id.organiser);
        organiser_address = (EditText) findViewById(R.id.organiser_address);
        organiser_contact = (EditText) findViewById(R.id.organiser_contact);
        organiser_email = (EditText) findViewById(R.id.organiser_email);
        organiser_website = (EditText) findViewById(R.id.organiser_website);
        date_value = (TextView) findViewById(R.id.date_value);
        time_value = (TextView) findViewById(R.id.time_value);
        add_post = (TextView) findViewById(R.id.add_post);
        contentImage = (CircleImageView) findViewById(R.id.contentImage);
        addPostLayout = (RelativeLayout) findViewById(R.id.submit_post_layout);
        contentImage.setOnClickListener(this);
        addPostLayout.setOnClickListener(this);
        date_value.setOnClickListener(this);
        time_value.setOnClickListener(this);
        testing = (ImageView) findViewById(R.id.testing);
        speakerName.setTypeface(GlobalConfig.FontType(this, 6));
        topic.setTypeface(GlobalConfig.FontType(this, 6));
        venue.setTypeface(GlobalConfig.FontType(this, 6));
        soz_salam.setTypeface(GlobalConfig.FontType(this, 6));
        shayar.setTypeface(GlobalConfig.FontType(this, 6));
        nauhakhwani.setTypeface(GlobalConfig.FontType(this, 6));
        organiser.setTypeface(GlobalConfig.FontType(this, 6));
        organiser_address.setTypeface(GlobalConfig.FontType(this, 6));
        organiser_contact.setTypeface(GlobalConfig.FontType(this, 6));
        organiser_email.setTypeface(GlobalConfig.FontType(this, 6));
        organiser_website.setTypeface(GlobalConfig.FontType(this, 6));
        date_value.setTypeface(GlobalConfig.FontType(this, 6));
        time_value.setTypeface(GlobalConfig.FontType(this, 6));
        add_post.setTypeface(GlobalConfig.FontType(this, 6));


        venue.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (venue.getRight() - venue.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        Intent n = new Intent(ContentVisualizingScreen.this, CreateVenueScreen.class);
                        startActivity(n);
                        return true;
                    }
                }
                return false;
            }
        });


        LatLng latLng = new LatLng(26.8465, 80.9467);
        venue.setEnabled(true);
        venue.setAdapter(new PlacesAutoCompleteAdapter("Venue", latLng, ContentVisualizingScreen.this, R.layout.autocomplete_list_item));
        venue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PlacesModel placesModelDesired = (PlacesModel) adapterView.getItemAtPosition(i);
                venue.setText(placesModelDesired.getDescription());
                venueCityName = placesModelDesired.getCity();
                venueStateName = placesModelDesired.getState();
                venueCountryName = placesModelDesired.getCountry();
                venueLocationName = placesModelDesired.getDesiredLocation();
                venueLocationPlaceId = placesModelDesired.getPlacesId();

                venue.setEnabled(false);

                Places.GeoDataApi.getPlaceById(mGoogleApiClient, venueLocationPlaceId)
                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (places.getStatus().isSuccess()) {
                                    final Place myPlace = places.get(0);
                                    LatLng queriedLocation = myPlace.getLatLng();
                                    String address = myPlace.getAddress().toString();
                                    venueLatitude = queriedLocation.latitude;
                                    venueLongitude = queriedLocation.longitude;
                                }
                                places.release();
                            }
                        });
            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contentImage:
                Intent nextScreen = new Intent(ContentVisualizingScreen.this,
                        SelectImageScreen.class);
                startActivityForResult(nextScreen, 1000);
                break;

            case R.id.submit_post_layout:
                boolean validateStatus = validateDetails();
                if (validateStatus) {
                    String dateTimeFormat = globalDate + " " + globalTime;
                    SimpleDateFormat sdfTime = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    try {
                        Date date = sdfTime.parse(dateTimeFormat);
                        long unix = date.getTime() / 1000;
                        Log.e("datetime", unix + "");
                        unixTimeStamp = unix + "";
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.template01);
                    String[] details = {
                            speakerName.getText().toString(),
                            dateTimeFormat,
                            venue.getText().toString(),
                            "Soz-e-Salam:\n" + soz_salam.getText().toString(),
                            "Shayar:\n" + shayar.getText().toString(),
                            "Nauhakhwan:\n" + nauhakhwani.getText().toString(),
                            organiser.getText().toString() + organiser_address.getText().toString() +
                                    organiser_contact.getText().toString() +
                                    organiser_email.getText().toString() +
                                    organiser_website.getText().toString(),
                            "",
                    };
                    float[] detailsF = {400f, 170f, 170f, 30f, 30f, 30f, 20f, 400f};
                    float[] detailsFi = {910f, 473f, 443f, 740f, 770f, 800f, 1000f, 633f};
                    mProgressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
                    mProgressDialog.setMessage("Validating...");
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();

                    //324,326
                    Bitmap bitmap = makeDetailedBitmap(icon, details, detailsF, detailsFi);
                    if (bitmap != null) {
                        mProgressDialog.cancel();
                        testing.setVisibility(View.VISIBLE);
                        testing.setImageBitmap(bitmap);
                    } else {
                        mProgressDialog.cancel();
                    }

                    postData(bitmap);
                }
                break;

            case R.id.date_value:
                datePicker();
                break;

            case R.id.time_value:
                timePicker();
                break;

        }
    }

    private void datePicker() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH); // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);
        CalendarDay calendarDay = new CalendarDay(year, month, day);
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setPreselectedDate(year, month, day)
                .setDateRange(calendarDay, null)
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeDark();
        cdp.show(getSupportFragmentManager(), "DATE_PICKER");
    }

    private void timePicker() {
        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(this)
                .setStartTime(10, 10)
                .setDoneText("Ok")
                .setCancelText("Cancel")
                .setThemeDark();
        rtpd.show(getSupportFragmentManager(), "TIME_PICKER");

    }

    /**
     * This method is used to upload the content media to server.
     *
     * @param contentMediaPath - Content Media Path
     */
    private void makeUploadContentMediaRequest(final String contentMediaPath) {
        try {
            if (contentMediaPath != null && contentMediaPath.length() > 0) {
                // Show progress dialog
                mProgressDialog =new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
                mProgressDialog.setMessage("Validating...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

//                ProgressDialog progressDialog = new Progr.Builder(context)
//                        .content("Please wait")
//                        .progress(true, 0)
//                        .show();
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

//                // Make the ByteArray for the content media
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                contentMedia.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
//                byte[] imageByteArray = byteArrayOutputStream.toByteArray();

                // HashMap for the header
                HashMap<String, String> header = new HashMap<>();
                header.put("content-type", "image/png");

                // HashMap for the parameters
                HashMap<String, String> params = new HashMap<>();
                header.put("image", "img1.png");

                // HashMap for the content media
//                Map<String, Object> mediaToUpload = new HashMap<>();
//                mediaToUpload.put(GlobalServiceConfig.SPK_DATA, imageByteArray);

                Map<String, Object> mediaToUpload = new HashMap<>();
                File mediaFile = new File(contentMediaPath);
                mediaToUpload.put("image", mediaFile);

                APIManager.makeUploadContentMediaRequest(context, mProgressDialog, params, header, mediaToUpload,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (context != null) {
                                        // publish the the content media to channel
                                        // S3 Media Url
                                        if (response.has(GlobalConfig.MEDIA_URL) && !response.getString(GlobalConfig.MEDIA_URL).equalsIgnoreCase("null")) {
                                            String s3Url = response.getString(GlobalConfig.MEDIA_URL);

                                            if (s3Url.length() > 0) {
//                                                if (loggedInUserId > 0 && channelId != null && channelId.length() > 0
//                                                        && loggedInUserFirstName != null && loggedInUserFirstName.length() > 0
//                                                        && loggedInUserLastName != null && loggedInUserLastName.length() > 0) {

                                                    // Get Image Orientation
                                                    ExifInterface ei = new ExifInterface(contentMediaPath);
                                                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                                    int imageOrientation = -1;
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inJustDecodeBounds = true;
                                                    BitmapFactory.decodeFile(contentMediaPath, options);
                                                    int imageHeight = options.outHeight;
                                                    int imageWidth = options.outWidth;


                                                }
                                            } else {
                                                // Show message for empty message
                                                Toast.makeText(context, "Failed to uplaod",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    } catch (JSONException e1) {
                                    if (mProgressDialog != null && mProgressDialog.isShowing())
                                        mProgressDialog.dismiss();
                                    e1.printStackTrace();
                                } catch (IOException e1) {
                                    if (mProgressDialog != null && mProgressDialog.isShowing())
                                        mProgressDialog.dismiss();
                                    e1.printStackTrace();
                                }
                                // Dismiss the progress dialog if visible
                                    if (mProgressDialog != null && mProgressDialog.isShowing())
                                        mProgressDialog.dismiss();

                                    // Reset the capture image path
                                    imagePath = null;


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                // Handle the error
                                if (context != null)
                                    APIManager.handleErrorResponseOfRequest(context, error, "Error",
                                            "Fail Upload Content Media Request");
                                // Dismiss the progress dialog if visible
                                if (mProgressDialog != null && mProgressDialog.isShowing())
                                    mProgressDialog.dismiss();

                                // Reset the capture image path
                                imagePath = null;
                            }
                        });
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "makeUploadContentMediaRequest Exception");
            e.printStackTrace();

            // Dismiss the progress dialog if visible
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();

            // Reset the capture image path
            imagePath = null;
        }
    }

    //template is the card template, strings is the details, x and y are the locations of the strings.
//The top left corner of the image is (0, 0), and the bottom right is (template.width, template.height)
    private Bitmap makeDetailedBitmap(final Bitmap template, final String[] strings, final float[] x, final float[] y) {
        Bitmap result = Bitmap.createBitmap(template.getWidth(), template.getHeight(), template.getConfig());   //Create the base image
        Canvas canvas = new Canvas(result);   //Create a canvas so we can draw onto the base image
        canvas.drawBitmap(template, 0, 0, null);   //Draw the template


        //EDIT: Forgot to set the text paint
        Paint p = new Paint();
        //p.setTextAlign(Paint.Align.CENTER);   //Uncomment if you want centered text

        //Change as you please
        p.setTypeface(GlobalConfig.FontType(ContentVisualizingScreen.this, 6));
        p.setColor(Color.WHITE);   //Change as you please


        final int len = strings.length; //Assumes that the length of x and y are >= the length of strings
        for (int i = 0; i < len; i++) {
//            if (i == 4) {
            p.setTextSize(27);
//            } else {
//                p.setTextSize(17);
//            }

            if (i == len - 1) {
                canvas.drawBitmap(contentImageBitmap, x[i], y[i], p);
            } else {
                String tobeDrawn = strings[i];
                float stringLength = p.measureText(tobeDrawn);
                stringLength = stringLength + x[i];
                Log.e("string", "string: " + stringLength);
                if (stringLength > 605 && i == 2) {
                    getCorrectAlignment(tobeDrawn, 0, x[i], stringLength, p);
                    recursiveLogicCounter = 0;
                    String firstLine = tobeDrawn.substring(0, positionBreakPoint);
                    String nextLine = tobeDrawn.substring(positionBreakPoint + 1, tobeDrawn.length());
                    canvas.drawText(firstLine, x[i], y[i], p);
                    canvas.drawText(nextLine, x[i], y[i] + 23, p);
                } else {
                    canvas.drawText(strings[i], x[i], y[i], p);//Draws text onto image
                }

            }

        }
        return result;   //Returns image that was modified using the canvas (aka image with details)
    }

    private void getCorrectAlignment(String s, int caughtCounter, float x, float length, Paint p) {
        int position = 0;
        int c = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            char d = s.charAt(i);
            if (d == ' ') {
                if (caughtCounter == c) {
                    position = i;
                    break;
                }
                c++;
            }
        }
        if (p.measureText(s.substring(0, position)) + x < 605) {
            positionBreakPoint = position;
        } else {
            recursiveLogicCounter++;
            getCorrectAlignment(s, recursiveLogicCounter, x, 605, p);
        }
    }

    private void postData(Bitmap bitmap) {

        JSONObject obj = new JSONObject();
        try {
            String details = "Soz_Salam: "+soz_salam+", Nauhakhwan: "+nauhakhwani+", Shayar: "+shayar+
                    ", Organizer: "+organiser.getText().toString()+organiser_address.getText().toString()
                    + organiser_contact.getText().toString() + organiser_website.getText().toString();
            obj.put("Description", "Majlis by: "+ speakerName.getText().toString());
            obj.put("Image", getStringImage(bitmap));
            obj.put("Category", "test");
            obj.put("CategoryId", 1);
            obj.put("UserId", Integer.parseInt(GlobalConfig.getSharedValuePreference(context,"UserId")));
            obj.put("Privacy", "public");
            obj.put("DesiredLocation", GlobalConfig.getSharedValuePreference(context,"DesiredLocation"));
            obj.put("DesiredLocationId", GlobalConfig.getSharedValuePreference(context,"DesiredLocationId"));
            obj.put("Status", true);
            obj.put("VenueId", 0);
            obj.put("VenueName", venue.getText().toString());
            obj.put("Details", details);
            obj.put("StartDateTime", unixTimeStamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        // APIManager.jsonObjectVolleyRequest(ContentVisualizingScreen.this,"", obj);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 75, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private boolean validateDetails() {
        if (speakerName.getText().toString().trim().equals("")) {
            showToastMessage("Speaker Name cannot be blank");
            return false;
        } else if (topic.getText().toString().trim().equals("")) {
            showToastMessage("Topic cannot be blank");
            return false;
        } else if (venue.getText().toString().trim().equals("")) {
            showToastMessage("Venue cannot be blank");
            return false;
        } else if (organiser.getText().toString().trim().equals("")) {
            showToastMessage("Organiser cannot be blank");
            return false;
        } else if (organiser_contact.getText().toString().trim().equals("")) {
            showToastMessage("Organiser Contact cannot be blank");
            return false;
        } else {
            return true;
        }
    }


    private void dateShowDialog() {

    }

    private void showToastMessage(String message) {
        Toast.makeText(ContentVisualizingScreen.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1000:
                if (data.hasExtra("ImageURI")) {
                    Uri imageUri = Uri.parse(data.getStringExtra("ImageURI"));
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    performCropImage(imageUri.getPath());
//                    Glide.with(ContentVisualizingScreen.this)
//                            .load(new File(imageUri.getPath()))
//                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                            .into(contentImage);
                }
                if (data.hasExtra("CameraImageURI")) {
                    String uri = data.getStringExtra("CameraImageURI");

                    Uri imageUri = Uri.parse(uri);
                    String pathOriginal = getRealPathFromUri(ContentVisualizingScreen.this, imageUri);
                    Toast.makeText(ContentVisualizingScreen.this, pathOriginal, Toast.LENGTH_SHORT).show();
//                    Glide.with(ContentVisualizingScreen.this)
//                            .load(new File(pathOriginal))
//                            .diskCacheStrategy( DiskCacheStrategy.RESULT )
//                            .into(contentImage);
                }
                if (data.hasExtra("Error")) {
                    Toast.makeText(ContentVisualizingScreen.this, data.getStringExtra("Error"), Toast.LENGTH_SHORT).show();
                }

                break;
            case 400:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
//                    Bitmap selectedBitmap = extras.getParcelable("data");
//                    Uri imageSelected = data.getParcelableExtra(Intent.EXTRA_STREAM);
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mCropImagedUri);
//                        imageView.setImageBitmap(bitmap);
                        contentImage.setImageBitmap(bitmap);
                        contentImageBitmap = bitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//
                    // Set The Bitmap Data To ImageView
//                    Uri imageUri = Uri.parse(data.getData());
//                    Glide.with(ContentVisualizingScreen.this)
//                            .load(new File(data.getData().getPath()))
//                            .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                            .into(contentImage);
                }
                break;


        }

    }

    private boolean performCropImage(String mFinalImageUri) {
        try {
            if (mFinalImageUri != null) {
                //call the standard crop action intent (the user device may not support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                //indicate image type and Uri
                File fILE = new File(mFinalImageUri);
                Uri contentUri = Uri.fromFile(fILE);
                cropIntent.setDataAndType(contentUri, "image/*");
                //set crop properties
                cropIntent.putExtra("crop", "true");
                //indicate aspect of desired crop
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("scale", true);
                //indicate output X and Y
                cropIntent.putExtra("outputX", 209);
                cropIntent.putExtra("outputY", 209);
                //retrieve data on return
                cropIntent.putExtra("return-data", false);

                File f = createNewFile("CROP_");
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Log.e("io", ex.getMessage());
                }

                mCropImagedUri = Uri.fromFile(f);
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
                //start the activity - we handle returning in onActivityResult
                startActivityForResult(cropIntent, RESULT_CROP);
                return true;
            }
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return false;
    }

    private File createNewFile(String prefix) {
        if (prefix == null || "".equalsIgnoreCase(prefix)) {
            prefix = "IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory() + "/Waseelah/");
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d(getClass().getName(), newDirectory.getAbsolutePath() + " directory created");
            }
        }
        File file = new File(newDirectory, (prefix + System.currentTimeMillis() + ".jpg"));
        if (file.exists()) {
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 0);
            cropIntent.putExtra("aspectY", 0);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 161);
            cropIntent.putExtra("outputY", 161);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        String dayOfMonthString = dayOfMonth + "";
        String monthOfYearString = (monthOfYear + 1) + "";
//        String localDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
        Log.e("Day: ", numberOfDigits(dayOfMonth) + "");
        Log.e("Month: ", numberOfDigits(monthOfYear) + "");

        if (numberOfDigits(dayOfMonth) == 1) {

            dayOfMonthString = "0" + dayOfMonthString;
        }
        if (numberOfDigits((monthOfYear + 1)) == 1) {
            monthOfYearString = "0" + monthOfYearString;
        }

        globalDate = dayOfMonthString + "-" + monthOfYearString + "-" + year;
        date_value.setText(globalDate);
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String hourOfDayString = hourOfDay + "";
        String minuteString = minute + "";
        if (numberOfDigits(hourOfDay) == 1) {
            hourOfDayString = "0" + hourOfDay;
        }
        if (numberOfDigits(minute) == 1) {
            minuteString = "0" + minute;
        }

        globalTime = hourOfDayString + ":" + minuteString;
        time_value.setText(globalTime);
    }

    private int numberOfDigits(int n) {
        int c = 0;
        while (n != 0) {
            int d = n % 10;
            c++;
            n = n / 10;
        }
        return c;
    }
}
