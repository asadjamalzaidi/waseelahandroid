package com.waseelah;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.waseelah.DesiredLocation.DesiredContainer;
import com.waseelah.Utilities.GlobalConfig;

/**
 * Created by training on 08/08/16.
 */

public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                String MobileNumber = GlobalConfig.getSharedValuePreference(SplashScreen.this, "MobileNumber");
                if (!MobileNumber.equals("")) {
                    intent = new Intent(SplashScreen.this, DesiredContainer.class);
                } else {
                    intent = new Intent(SplashScreen.this, LoginScreen.class);
                }
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
