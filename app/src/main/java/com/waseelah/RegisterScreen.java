package com.waseelah;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import com.waseelah.Adapter.PlacesAutoCompleteAdapter;
import com.waseelah.Model.PlacesModel;
import com.waseelah.Utilities.APIManager;
import com.waseelah.Utilities.GlobalConfig;
import com.waseelah.Utilities.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by training on 08/08/16.
 */

public class RegisterScreen extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private AutoCompleteTextView autoCompleteDesired;
    private GoogleApiClient mGoogleApiClient;
    private AutoCompleteTextView autocompleteViewCity;
    private PlacesModel placesModel;
    private String userCityName = "", userStateName = "", userCountryName = "", userPostalCode = "",
            desiredLocationName = "", desiredLocationPlaceId = "", desiredCityName = "",
            desiredStateName = "", desiredCountryName = "", desiredPostalCode = "";
    private double desiredLatitude = 0.0, desiredLongitude = 0.0;
    private EditText firstName, lastName, emailAddress, mobileNumber, password;
    private Button buttonSubmit;
    private boolean passwordShow = false, citySelected = false, desiredLocationSelected = false;
    private ImageView editCity, editDesiredLocation;
    private TextView textGender;
    private RadioGroup radioGroup;
    private RadioButton radioMale, radioFemale, radioSexButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        // emailAddress = (EditText) findViewById(R.id.emailAddress);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        password = (EditText) findViewById(R.id.password);
        autoCompleteDesired = (AutoCompleteTextView) findViewById(R.id.desiredLocation);
        autocompleteViewCity = (AutoCompleteTextView) findViewById(R.id.city);
        buttonSubmit = (Button) findViewById(R.id.btnSubmit);
        editCity = (ImageView) findViewById(R.id.editCity);
        editDesiredLocation = (ImageView) findViewById(R.id.editDesiredLocation);
        textGender = (TextView) findViewById(R.id.gender);
        radioGroup = (RadioGroup) findViewById(R.id.radioSex);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);
        assignTextFace();

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addOnConnectionFailedListener(this)
                .build();
        getCityList();

        autoCompleteDesired.setEnabled(false);
        buttonSubmit.setOnClickListener(this);
        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        if (!passwordShow) {
                            password.setTransformationMethod(null);
                            password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_noview, 0);
                            passwordShow = true;
                        } else {
                            password.setTransformationMethod(new PasswordTransformationMethod());
                            password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_view, 0);
                            passwordShow = false;
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void assignTextFace() {
        firstName.setTypeface(GlobalConfig.FontType(this, 6));
        lastName.setTypeface(GlobalConfig.FontType(this, 6));
        // emailAddress.setTypeface(GlobalConfig.FontType(this, 6));
        mobileNumber.setTypeface(GlobalConfig.FontType(this, 6));
        password.setTypeface(GlobalConfig.FontType(this, 6));
        autoCompleteDesired.setTypeface(GlobalConfig.FontType(this, 6));
        autocompleteViewCity.setTypeface(GlobalConfig.FontType(this, 6));
        buttonSubmit.setTypeface(GlobalConfig.FontType(this, 6));
        textGender.setTypeface(GlobalConfig.FontType(this, 6));
        radioMale.setTypeface(GlobalConfig.FontType(this, 6));
        radioFemale.setTypeface(GlobalConfig.FontType(this, 6));
    }

    private void getCityList() {
        autocompleteViewCity.setAdapter(new PlacesAutoCompleteAdapter("City", null, RegisterScreen.this, R.layout.autocomplete_list_item));
        autocompleteViewCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                placesModel = (PlacesModel) adapterView.getItemAtPosition(i);
                autocompleteViewCity.setText(placesModel.getDescription());
                userCityName = placesModel.getCity();
                userStateName = placesModel.getState();
                userCountryName = placesModel.getCountry();
                autocompleteViewCity.setEnabled(false);
                editCity.setVisibility(View.VISIBLE);
                citySelected = true;
                editCity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        autocompleteViewCity.setEnabled(true);
                        autocompleteViewCity.setText("");
                        autoCompleteDesired.setEnabled(false);
                        autoCompleteDesired.setText("");
                        editCity.setVisibility(View.GONE);
                        citySelected = false;
                    }
                });
                Places.GeoDataApi.getPlaceById(mGoogleApiClient, placesModel.getPlacesId())
                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(final PlaceBuffer places) {
                                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                    final Place myPlace = places.get(0);
                                    Log.i("TAG", "Place found: " + myPlace.getName());
                                    LatLng latLng = myPlace.getLatLng();
                                    String address = myPlace.getAddress().toString();
                                    userPostalCode = address.replaceAll("\\D+", "");
                                    autoCompleteDesired.setEnabled(true);
                                    autoCompleteDesired.setAdapter(new PlacesAutoCompleteAdapter("Location", latLng, RegisterScreen.this, R.layout.autocomplete_list_item));
                                    autoCompleteDesired.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            PlacesModel placesModelDesired = (PlacesModel) adapterView.getItemAtPosition(i);
                                            autoCompleteDesired.setText(placesModelDesired.getDescription());
                                            desiredCityName = placesModelDesired.getCity();
                                            desiredStateName = placesModelDesired.getState();
                                            desiredCountryName = placesModelDesired.getCountry();
                                            desiredLocationName = placesModelDesired.getDesiredLocation();
                                            desiredLocationPlaceId = placesModelDesired.getPlacesId();

                                            autoCompleteDesired.setEnabled(false);
                                            editCity.setVisibility(View.GONE);
                                            editDesiredLocation.setVisibility(View.VISIBLE);
                                            desiredLocationSelected = true;
                                            editDesiredLocation.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    autoCompleteDesired.setEnabled(true);
                                                    autoCompleteDesired.setText("");
                                                    editDesiredLocation.setVisibility(View.GONE);
                                                    editCity.setVisibility(View.VISIBLE);
                                                    desiredLocationSelected = false;
                                                }
                                            });
                                            Places.GeoDataApi.getPlaceById(mGoogleApiClient, desiredLocationPlaceId)
                                                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                                        @Override
                                                        public void onResult(PlaceBuffer places) {
                                                            if (places.getStatus().isSuccess()) {
                                                                final Place myPlace = places.get(0);
                                                                LatLng queriedLocation = myPlace.getLatLng();
                                                                String address = myPlace.getAddress().toString();
                                                                desiredPostalCode = address.replaceAll("\\D+", "");
                                                                desiredLatitude = queriedLocation.latitude;
                                                                desiredLongitude = queriedLocation.longitude;
                                                            }
                                                            places.release();
                                                        }
                                                    });

                                        }
                                    });
                                } else {
                                    Log.e("TAG", "Place not found");
                                }
                                places.release();
                            }
                        });

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                boolean fieldStatus = validateDetails();
                if (fieldStatus) {
                    validateRegistration();
                }
                break;
        }
    }

    private boolean validateDetails() {
        if (firstName.getText().toString().equals("")) {
            showToastMessage("Name cannot be blank");
            return false;
        } else if (mobileNumber.getText().toString().equals("")) {
            showToastMessage("Mobile cannot be blank");
            return false;
        } else if (password.getText().toString().equals("")) {
            showToastMessage("Password cannot be blank");
            return false;
        } else if (autoCompleteDesired.getText().toString().equals("")) {
            showToastMessage("Location cannot be blank");
            return false;
        } else if (autocompleteViewCity.getText().toString().equals("")) {
            showToastMessage("City cannot be blank");
            return false;
        } else if (!citySelected) {
            showToastMessage("Please select city from the suggestions");
            return false;
        } else if (!desiredLocationSelected) {
            showToastMessage("Please select location from the suggestions");
            return false;
        } else {
            return true;
        }
    }

    private void showToastMessage(String message) {
        Toast.makeText(RegisterScreen.this, message, Toast.LENGTH_SHORT).show();
    }

    private void validateRegistration() {
        // get selected radio button from radioGroup
        int selectedId = radioGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        radioSexButton = (RadioButton) findViewById(selectedId);
        String sexSelection = (String) radioSexButton.getText();

        String Url = GlobalConfig.BASE_URL + GlobalConfig.REGISTER_METHOD;
        String passwordCalculated = GlobalConfig.encodePassword(password.getText().toString());
        Map<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("FirstName", firstName.getText().toString());
        requestParams.put("LastName", lastName.getText().toString());
        requestParams.put("MiddleName", "");
        requestParams.put("MobileNumber", mobileNumber.getText().toString());
        requestParams.put("Email", "");
//        requestParams.put("Gender", sexSelection);
        requestParams.put("UserCity", userCityName);
        requestParams.put("UserState", userStateName);
        requestParams.put("UserCountry", userCountryName);
        requestParams.put("UserPostalCode", userPostalCode);
        requestParams.put("DesiredLocation", desiredLocationName);
        requestParams.put("DesiredLocationPlaceId", desiredLocationPlaceId);
        requestParams.put("DesiredLatitude", desiredLatitude + "");
        requestParams.put("DesiredLongitude", desiredLongitude + "");
        requestParams.put("DesiredCity", desiredCityName);
        requestParams.put("DesiredState", desiredStateName);
        requestParams.put("DesiredCountry", desiredCountryName);
        requestParams.put("DesiredPostalCode", desiredPostalCode);
        requestParams.put("Password", passwordCalculated);
        APIManager.stringVolleyRequest("RegisterScreen", Url, "POST", requestParams, RegisterScreen.this);
        APIManager.setOnAPICallbackListener(new RequestCallback.APIRequestCallback() {
            @Override
            public void onSuccessResponse(String response) {
                Log.e("response", response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.has("MobileNumber") && !obj.isNull("MobileNumber")) {
                        Intent n = new Intent(RegisterScreen.this, VerifyMobileScreen.class);
                        n.putExtra("MobileNumber", obj.getString("MobileNumber"));
                        startActivity(n);
                        finish();
                    } else {

                        Toast.makeText(RegisterScreen.this, "Error in saving details. Try later", Toast.LENGTH_SHORT).show();
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onSuccessJSONResponse(JSONObject response) {

            }

            @Override
            public void onSuccessJSONArrayResponse(JSONArray response) {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                APIManager.handleErrorResponseOfRequest(RegisterScreen.this, error, "Server Error", "LoginScreen");
            }

            @Override
            public void statusCode(int statusCode) {
            }
        });

    }


}
